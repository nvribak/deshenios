//
//  SignUpViewController.m
//  Deshsen
//
//  Created by Avi Osipov on /17/714.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "SignUpViewController.h"

@interface SignUpViewController ()

@end

@implementation SignUpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [TinyHelpers addTextFieldPadding:self.emailField];
    [TinyHelpers addTextFieldPadding:self.passField];
    [TinyHelpers addTextFieldPadding2:self.nameField];
    [TinyHelpers addTextFieldPadding:self.phoneField];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField.tag == 2)
        [self animateTextField: textField up: YES];
    if (textField.tag ==3)
        [self animateTextField2: textField up: YES];

}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag == 2)
        [self animateTextField: textField up: NO];
    if (textField.tag ==3)
        [self animateTextField2: textField up: NO];

    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 80; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (void) animateTextField2: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 120; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)signUpBtn:(id)sender {
    
    
    if ([self.emailField.text isEqualToString:@""] ||
        
       
        [self.passField.text isEqualToString:@""]
        
        
        )
        [TinyHelpers showAlert:@"הרשמה" withMessage:@"עליך להזין כתובת מייל וסיסמה"];
    
    
    
    
    else
    {
            
      if ( [UserDataManager signupUserwithEmail:self.emailField.text andName:self.nameField.text andPhone:self.phoneField.text andPassword:self.passField.text])
      {
          [self dismissViewControllerAnimated:YES completion:nil];
          [TinyHelpers showAlert:@"הרשמה" withMessage:@"הרשמתך התבצעה בהצלחה"];

          
      }
  
        else
            
            [TinyHelpers showAlert:@"הרשמה" withMessage:@"לא ניתן לרשום את המשתמש.נסה שנית"];

        
    }
    
    
    
    
}

- (IBAction)cancelBtn:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}
@end
