//
//  FertilizerViewController.h
//  Deshsen
//
//  Created by Avi Osipov on /19/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "KeyboardSupportViewController.h"
#import "FertilizerCell.h"
#import "LandDataItem.h"
#import "FarmDataItem.h"
#import "FertilizerDataManager.h"
#import "FertilizerDataItem.h"
#import "FertilizerTypeDataItem.h"
@interface FertilizerViewController : KeyboardSupportViewController <UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UIPickerViewDelegate ,UIPickerViewDataSource>{
    int selectedIndex;
    UIPickerView *pickerView ;
    NSArray *fertilizersArray;
    FertilizerDataManager * manager;
    NSMutableSet *fertilizerType;
    NSMutableArray *sortedFertilizerTypesArray;
    NSString * fertilizerOrFertilizerAmountOrFertilizerWater;
    NSMutableArray * fertilizerAmountArray;
    NSMutableArray * fertilizerWaterArray;
    
    NSString * fertType;
    NSString * fertilierAmount;
    NSString * waterAmount;
    
    NSArray * MatchResultFertilizers;
    BOOL fertilizerWasAddedToLand;
    FertilizerDataItem * selectedFertilizer;
    

    
}
@property (strong) LandDataItem * landItem;
@property (strong) FarmDataItem * farmItem;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIPickerView *picker;
- (IBAction)pickFertilizerBtn:(id)sender;
- (IBAction)pickFertilizerAmountBtn:(id)sender;
- (IBAction)pickWaterAmountBtn:(id)sender;
- (IBAction)doSaveHistoryBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *fertilizerLabel;

@property (strong, nonatomic) IBOutlet UIButton *fertAmountLabel;
@property (strong, nonatomic) IBOutlet UIButton *waterAmountLabel;

@property (strong, nonatomic) IBOutlet UIView *OrderFertilizerView;
- (IBAction)closeOrderView:(id)sender;

- (IBAction)MailOrderToAdmin:(id)sender;

@property (strong, nonatomic) IBOutlet UITextView *mailBodyText;
@property (strong, nonatomic) IBOutlet UILabel *orderViewTitle;

@end
