//
//  EditAccountViewController.m
//  Deshsen
//
//  Created by Avi Osipov on /20/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "EditAccountViewController.h"

@interface EditAccountViewController ()

@end

@implementation EditAccountViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [TinyHelpers addTextFieldPadding2:self.fullNameField ] ;
    self.fullNameField.delegate = self;
    [TinyHelpers addTextFieldPadding2:self.addressField ] ;
    self.addressField.delegate = self;
    [TinyHelpers addTextFieldPadding:self.phoneField ] ;
    self.phoneField.delegate = self;
    [TinyHelpers addTextFieldPadding:self.emailField ] ;
    self.emailField.delegate = self;

	// Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = @"עדכון פרטי חשבון";
    
    self.navigationItem.hidesBackButton = YES;
    
    UIButton *butt=[UIButton buttonWithType:UIButtonTypeCustom ];
    [butt setFrame:CGRectMake(2, 2, 40, 40)];
    [butt setImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    
    [butt addTarget:self action:@selector(_doGoBack) forControlEvents:UIControlEventTouchDown];
    [butt setTag:100];
    
    
    //  self.navigationItem.leftBarButtonItem = leftButton;
    [self.navigationController.navigationBar addSubview:butt];
    
    if (self.user!=nil)
    {
        
        /*
         @property (strong, nonatomic) IBOutlet UITextField *fullNameField;
         @property (strong, nonatomic) IBOutlet UITextField *addressField;
         @property (strong, nonatomic) IBOutlet UITextField *phoneField;
         @property (strong, nonatomic) IBOutlet UITextField *emailField;
         */
        self.fullNameField.text = self.user.fullName;
        self.phoneField.text = self.user.phone;
        self.emailField.text = self.user.email;
        
    }
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)_doGoBack
{
    [self clearBarButtons];
    [self.navigationController popViewControllerAnimated:YES];
    
}
    -(void)clearBarButtons
    {
        for (UIView *subView in self.navigationController.navigationBar.subviews)
        {
            if (subView.tag == 100 || subView.tag ==200 || subView.tag == 300)
                
            {
                [subView removeFromSuperview];
            }
        }
        
    }

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}




- (IBAction)saveChangesBtn:(id)sender {
    
    UserDataItem * updateUser = [[UserDataItem alloc]init];
    updateUser._id = [UserDataManager getCurrentUser]._id;
    updateUser.fullName = self.fullNameField.text;
    updateUser.email = self.emailField.text;
    updateUser.phone = self.phoneField.text;
    
   if ([UserDataManager updateUserProfile:updateUser])
       [TinyHelpers showAlert:@"עדכון פרטי משתמש" withMessage:@"פרטי משתמש עודכנו בהצלחה במערכת."];
    else
        [TinyHelpers showAlert:@"עדכון פרטי משתמש" withMessage:@"פרטי משתמש לא עודכנו בהצלחה במערכת."];
    

}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField.tag == 3)
        [self animateTextField: textField up: YES];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag == 3)
        [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 80; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

@end
