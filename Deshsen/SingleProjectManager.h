//
//  SingleProjectManager.h
//  Deshsen
//
//  Created by Avi Osipov on /14/414.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SingleProjectManager : NSObject{
    UIImage *landPhotoImage ;
    

}
+ (SingleProjectManager *)sharedProject;
- (void)setPhotoImage: (UIImage *) photoImage;
- (UIImage *) getPhotoImage;


@end
