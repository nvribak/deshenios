//
//  FarmDataManager.m
//  Deshsen
//
//  Created by Avi Osipov on /6/414.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "FarmDataManager.h"

@implementation FarmDataManager

-(NSArray *)getFarmsByUserId: (NSString *)userId
{
    GodzillaDataManager * manager = [[GodzillaDataManager alloc]init];
    
    NSString *request = [NSString stringWithFormat:@"user_id=%@"  , userId] ;
    NSString *response = [manager sendServerRequest:@"get/farms" withGetQuery:request withPostQuery:@""] ;
    
    
    
    
    /// parse json response
    
    id jsonObj = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    
    
    if ([jsonObj isKindOfClass:[NSDictionary class]]) {
        
        
        
        NSArray *resultArray = [jsonObj objectForKey:@"data"];
        NSMutableArray *responseArray = [[NSMutableArray alloc] init ] ;
        
        for (NSDictionary *item in resultArray) {
            
            
            FarmDataItem *farmItem = [[FarmDataItem alloc] init] ;
            
            
            
            
            farmItem._id = [item objectForKey:@"_id"] ;
            farmItem.autoNumber = [[item objectForKey:@"_auto_number"] stringValue];
            farmItem.name = [item objectForKey:@"name"];
            farmItem.address = [item objectForKey:@"address"];
            farmItem.lat = [item objectForKey:@"latitude" ] ;
            farmItem.lng = [item objectForKey:@"longitude" ] ;
            if ([item objectForKey:@"phone"]!=nil)
                farmItem.phone = [NSString stringWithFormat:@"0%@",[item objectForKey:@"phone"]  ];
            farmItem.userId = [item objectForKey:@"user_id" ] ;
            farmItem.photo = [item objectForKey:@"photo"];
            
            
            
        //    NSString *date_created = [[item objectForKey:@"created"] objectForKey:@"sec"] ;
         //   NSDate *date = [TinyHelpers convertMongoDate:date_created] ;
            
        //   date = [TinyHelpers dateDiff:date ] ;
            
            
            [responseArray addObject:farmItem ] ;
            
            
            
        }
        
        
        
        
        return responseArray ;
    }
    return nil;
}

-(BOOL)addFarm:(FarmDataItem *)farm
{
    GodzillaDataManager *manager = [[GodzillaDataManager alloc] initWithAutoIncrement:YES] ;
    
    NSString *postQuery =@"";
    postQuery =   [ NSString stringWithFormat: @"&address=%@&latitude=%@&longitude=%@&name=%@&phone=%@&user_id=%@"
                   ,farm.address,farm.lat,farm.lng,farm.name,farm.phone,farm.userId] ;
    
    NSString * result =  [manager sendServerRequest:@"add/farms" withGetQuery:@"" withPostQuery:postQuery] ;
    
    /// parse json response
    
    id jsonObj = [NSJSONSerialization JSONObjectWithData:[result dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    
    
    NSLog(@"response : %@" , result ) ;
    id dataObj = [jsonObj objectForKey:@"success"] ;
    
    /// generate result array
    
    
    
    BOOL value =  [[dataObj objectForKey:@"ok"] boolValue]  ;
    if (value)
        return YES;
    return NO;
}


- (BOOL) updateFarm : (FarmDataItem *) farm
{
    

    
    GodzillaDataManager *manager = [[GodzillaDataManager alloc] init] ;
    
    NSString *postRequest = [NSString stringWithFormat:@"_id=%@&address=%@&latitude=%@&longitude=%@&phone=%@" ,
                              farm._id,farm.address,farm.lat,farm.lng,farm.phone] ;
    
    NSString *result = [ manager sendServerRequest:@"update/farms" withGetQuery:@"" withPostQuery:postRequest] ;
    
    
    
    
    /// parse json response
    
    id jsonObj = [NSJSONSerialization JSONObjectWithData:[result dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    
    NSLog(@"response : %@" , result ) ;
    id dataObj = [jsonObj objectForKey:@"success"] ;
    
    /// generate result array
    
    
    
    BOOL value =  [[dataObj objectForKey:@"ok"] boolValue]  ;
    if (value)
        return YES;
    return NO;
}

- (BOOL) updateFarmPhoto : (FarmDataItem *) farm
{
    
    
    
    GodzillaDataManager *manager = [[GodzillaDataManager alloc] init] ;
    
    NSString *postRequest = [NSString stringWithFormat:@"_id=%@&photo=%@" ,farm._id,farm.photo] ;
    
    NSString *result = [ manager sendServerRequest:@"update/farms" withGetQuery:@"" withPostQuery:postRequest] ;
    
    
    
    
    /// parse json response
    
    id jsonObj = [NSJSONSerialization JSONObjectWithData:[result dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    
    NSLog(@"response : %@" , result ) ;
    id dataObj = [jsonObj objectForKey:@"success"] ;
    
    /// generate result array
    
    
    
    BOOL value =  [[dataObj objectForKey:@"ok"] boolValue]  ;
    if (value)
        return YES;
    return NO;
}

@end
