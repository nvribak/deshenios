//
//  LandCell.h
//  Deshsen
//
//  Created by Avi Osipov on /18/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LandCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *landNum;

@property (strong, nonatomic) IBOutlet UILabel *specieName;
@property (strong, nonatomic) IBOutlet UILabel *assignedDate;
@property (strong, nonatomic) IBOutlet UIButton *openCam;


@end
