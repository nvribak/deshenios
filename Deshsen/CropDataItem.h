//
//  CropDataItem.h
//  Deshsen
//
//  Created by Avi Osipov on /7/414.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CropDataItem : NSObject

@property (strong,nonatomic) NSString * _id;
@property (strong,nonatomic) NSString * name;


@end
