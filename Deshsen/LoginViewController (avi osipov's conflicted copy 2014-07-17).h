//
//  LoginViewController.h
//  Deshsen
//
//  Created by Avi Osipov on /16/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "KeyboardSupportViewController.h"
#import "TinyHelpers.h"
#import "UserDataManager.h"
#import "SignUpViewController.h"

@interface LoginViewController : KeyboardSupportViewController<UITextFieldDelegate>{
    UserDataManager * manager;
}
@property (strong, nonatomic) IBOutlet UITextField *usernameField;
@property (strong, nonatomic) IBOutlet UITextField *passwordField;
- (IBAction)forgotPassBtn:(id)sender;
- (IBAction)signUpBtn:(id)sender;
- (IBAction)loginBtn:(id)sender;
- (IBAction)facebookBtn:(id)sender;

- (IBAction)openSignUpController:(id)sender;

@end
