//
//  TinyHelpers.m
//  WXG
//
//  Created by Avi Osipov on /6/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "TinyHelpers.h"

@implementation TinyHelpers
+ (void) showAlert : (NSString *) title withMessage : (NSString *) message
{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"ok"
                                          otherButtonTitles:nil];
    [alert show];
    
    
}



+ (NSString*)convertMongoDate:(id) unixDate {
    
    
    double unixTimeStamp = [unixDate doubleValue];
    
    NSTimeInterval _interval=unixTimeStamp;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd/MM/yyyy"];
    
    
    NSString *dateString = [format stringFromDate:date];
    return dateString;
    
    
    
    
}
+(NSString*)convertDateToString : (NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    
    NSString *string = [dateFormatter stringFromDate:date];
    return string;
}
+(NSString *)convertStringFromDate:(NSDate *)date
{
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd/MM/yyyy"];
    
        //Optionally for time zone converstions
      //  [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"..."]];
        
        NSString *stringFromDate = [formatter stringFromDate:date];
    
    return stringFromDate;
    }


+(NSString *)returnCurrentDate
{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd/MM/yyyy"];

NSDate *now = [[NSDate alloc] init];

NSString *dateString = [format stringFromDate:now];
    return dateString;
}


+ (NSString*)dateDiff:(NSDate*) date {
    
    
    
    NSDate* dateNow = [NSDate date] ;
    NSString* result ;
    
    NSTimeInterval distanceBetweenDates = [dateNow timeIntervalSinceDate:date];
    NSInteger minutesBetweenDates = distanceBetweenDates / 60 ;
    
    if (minutesBetweenDates > 59 ) {  // hours
        
        NSInteger hoursBetweenDates = minutesBetweenDates / 60 ;
        
        if (hoursBetweenDates > 12 ) {
            
            NSDateFormatter *formatter= [[NSDateFormatter alloc]init];
            [formatter setLocale:[NSLocale currentLocale]];
            [formatter setDateFormat:@"dd/MM/yyyy"];
            result = [ formatter stringFromDate:date];
            
        } else result = [[NSString alloc] initWithFormat:@"%d hours ago" , hoursBetweenDates ] ;
        
    } else result = [[NSString alloc] initWithFormat:@"%d minutes ago" , minutesBetweenDates ] ;
    
    
    
    return result ;
}

+(NSString *)calculateDateDifferenceInWeeks :(NSString *)date
{
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"dd/MM/yyyy"];
    NSDate *startDate = [NSDate date];
    NSLog(@"%@",startDate);
    NSDate *endDate = [f dateFromString:date];
    NSLog(@"%@",endDate);
    
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                        fromDate:endDate
                                                          toDate:startDate
                                                         options:0];
    int weeks = components.day/7+1;
    return [NSString stringWithFormat:@"%i",weeks];
   // return components.day/7;
}

+(NSString *)calculateDateDifferenceInWeeksFromDate :(NSString *)fromDate toDate :(NSString *)toDate

{
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"dd/MM/yyyy"];
    
    NSDate *startDate = [f dateFromString:fromDate];
    NSLog(@"%@",startDate);
    NSDate *endDate = [f dateFromString:toDate];
    NSLog(@"%@",endDate);
    
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    int days = components.day;
    return [NSString stringWithFormat:@"%i",days];
    // return components.day/7;
}

+(NSString *)fixDateFormat: (NSString *)date;
{
    
    NSDateFormatter *dformat = [[NSDateFormatter alloc]init];
    
    [dformat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    
    NSDate *date2 = [dformat dateFromString:date];
    
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    NSString * fixedDate = [formatter stringFromDate:date2];
    
    if (fixedDate==nil)
        return date;
    
    return fixedDate;
    
}

+(void) addTextFieldPadding:(UITextField*) textField
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(20, 0, 8, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    
}

+(void) addTextFieldPadding2:(UITextField*) textField
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(20, 0, 8, 20)];
    textField.rightView = paddingView;
    textField.rightViewMode = UITextFieldViewModeAlways;
    
}

@end
