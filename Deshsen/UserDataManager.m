//
//  UserDataManager.m
//  Deshsen
//
//  Created by Avi Osipov on /16/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "UserDataManager.h"

@implementation UserDataManager

- (id) init
{
    self = [super init];
    
    if (self != nil)
    {
        /// init stuff here
        
        NSLog(@"User Data Manager ready") ;
        
        
    }
    
    return self;
}

+ (BOOL) login : (NSString*) email Password : (NSString *) password
{
    
    GodzillaDataManager *manager = [[GodzillaDataManager alloc] init] ;
    
    NSString *request = [NSString stringWithFormat:@"email=%@&password=%@"  , email , password ] ;
    NSString *response = [manager sendServerRequest2:@"login" withGetQuery:@"" withPostQuery:request] ;
    
    
    
    
    /// parse json response
    
    id jsonObj = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    
    
    if ([jsonObj isKindOfClass:[NSDictionary class]]) {
        
        
        
        NSDictionary * data = [jsonObj objectForKey:@"data"];
        
        if ([data count]){
            
            /// set active user
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            
            [prefs setObject:[data objectForKey:@"_id"] forKey:@"userID"];
            [prefs setObject:[NSString stringWithFormat:@"0%@",[data objectForKey:@"phone"]] forKey:@"phone"];
            [prefs setObject:[data objectForKey:@"email"] forKey:@"email"];
            [prefs setObject:[data objectForKey:@"password"] forKey:@"password"];
            [prefs setObject:[data objectForKey:@"fullname"] forKey:@"fullName"];
            
            [prefs synchronize];
            
            
            
            return YES ;
            
            
            
            
        }
        else{
            
            
            /// user not found return failure
            
        //    [TinyHelpers showAlert:@"Login" withMessage:@"Account not found, please correct your credentials and try again."] ;
            
            
            return  NO ;
            
            
            
            
        }
        
    }
    else
        return NO;
    
}







+ (void) clearUserSession
{
    
    
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    
}

+ (UserDataItem *) getCurrentUser
{
    
    
    /*
     UserDataItem *userInfo = [[UserDataItem alloc] init] ;
     userInfo._id = @"52e018a2085868587ab96aa4" ;
     
     return  userInfo ;
     */
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if ([prefs stringForKey:@"userID"] != nil) {
        
        
        UserDataItem *userInfo = [[UserDataItem alloc] init] ;
        userInfo._id = [prefs stringForKey:@"userID"];
        userInfo.email = [prefs stringForKey:@"email"];
        userInfo.fullName = [prefs stringForKey:@"fullName"];
        userInfo.password = [prefs stringForKey:@"password"];
        userInfo.phone = [prefs stringForKey:@"phone"];
        
        
        
        return userInfo ;
        
    } else return nil ;
    

    
    
}

+ (BOOL) updateUserProfile : (UserDataItem *) user
{
    
    
    
    
    
    
    /// do not permit use the same email address twice
    //  NSString *validaitonQuery = [NSString stringWithFormat: @"query={\"email\" : \"%@\"} " , email] ;
    
    
    GodzillaDataManager *manager = [[GodzillaDataManager alloc] init] ;
    
    NSString *postRequest = [NSString stringWithFormat:@"_id=%@&fullname=%@&email=%@&phone=%@" ,
                             user._id , user.fullName,user.email,user.phone ] ;
    
    NSString *response = [ manager sendServerRequest:@"update/users" withGetQuery:@"" withPostQuery:postRequest] ;
    
    
    
    
    /// parse json response
    
    id jsonObj = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    
    
    if ([jsonObj isKindOfClass:[NSDictionary class]]) {
        
        
        
        
        if ( [[jsonObj objectForKey:@"success" ]  isKindOfClass:[NSDictionary class]] ) {
            
            /// set active user
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSDictionary *newData = [jsonObj objectForKey:@"data"] ;
            
            //   [prefs setObject:[newData objectForKey:@"_id"] forKey:@"userID"];
            [prefs setObject:[newData objectForKey:@"fullname"] forKey:@"fullName"];
            [prefs setObject:[newData objectForKey:@"email"] forKey:@"email"];
            [prefs setObject:[NSString stringWithFormat:@"0%@",[newData objectForKey:@"phone"] ] forKey:@"phone"];

            
            
            [prefs synchronize];
            
            
            //     [TinyHelpers showAlert:@"Registration" withMessage:@"Thank you for registering, welcome!"] ;
            
            
            /// return success
            
            return YES ;
            
            
            
            
        }
        else{
            
            //   [TinyHelpers showAlert:@"Registration" withMessage:@"Email is in use, please try another email address and try again."] ;
            
            /// user not found return failure
            
            return  NO ;
            
            
            
            
        }
        
    }
    
    
    return NO ;
    
    
    
}

+(BOOL) signupUserwithEmail: (NSString *) email andName:(NSString *)name andPhone: (NSString *)phone andPassword: (NSString *) password {
    
    
    
     
     /*
     
     שירות יצירת משתמש חדש:
     כתובת: http://deshen.appli.co.il/services/add_user
     צורת גישה: POST
     פרמטרי חובה: email, password.
     פרמטרי רשות: fullname, phone.
     תוצאה: שליחת דוא"ל עם פרטים.
     JSON חוזר:
     
     
     */

    
    GodzillaDataManager *manager = [[GodzillaDataManager alloc] init] ;
    
    NSString *postRequest = [NSString stringWithFormat:@"email=%@&password=%@&fullname=%@&phone=%@" ,
                             email,password,name,phone ] ;
    
    NSString *response = [ manager sendServerRequest2:@"add_user" withGetQuery:@"" withPostQuery:postRequest] ;
    
    
    
    
    /// parse json response
    
    id jsonObj = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    
    
    if ([jsonObj isKindOfClass:[NSDictionary class]]) {
        
        
        
        
        if ( [[jsonObj objectForKey:@"success" ]  isKindOfClass:[NSDictionary class]] ) {
            
            /// set active user
            
            
            
            
            
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSDictionary *newData = [jsonObj objectForKey:@"data"] ;
            [prefs setObject:[newData objectForKey:@"_id"] forKey:@"userID"];
            [prefs setObject:[newData objectForKey:@"fullname"] forKey:@"fullName"];
            [prefs setObject:[newData objectForKey:@"email"] forKey:@"email"];
            [prefs setObject:[NSString stringWithFormat:@"0%@",[newData objectForKey:@"phone"] ] forKey:@"phone"];
            
            
            
            
        
            
            
            [prefs synchronize];
            
            [self getCurrentUser];
          
            
            return YES ;
            
            
            
            
        }
        else{
            
         
            
            return  NO ;
            
            
            
            
        }
        
    }
    
    
    return NO ;
    
}

@end
