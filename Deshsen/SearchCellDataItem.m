//
//  SearchCellDataItem.m
//  ChamberApp
//
//  Created by Avi Osipov on /27/1113.
//
//

#import "SearchCellDataItem.h"

@implementation SearchCellDataItem



-(id)initWithTitle:(NSString *)title
{
    self = [super init];
    if (self) {
        
        self.title = title;
        self.selectedValue = @"" ;
        self.selectedValueKey = @"" ; 
        self.isEnabled = YES ;
        self.selectedPickerRow = 0 ;
        
    }
    return self;
}

@end
