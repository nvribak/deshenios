//
//  FertilizerDataItem.h
//  Deshsen
//
//  Created by Avi Osipov on /7/414.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FertilizerDataItem : NSObject

@property (strong,nonatomic) NSString * _id;
@property (strong,nonatomic) NSString * name;
@property (strong,nonatomic) NSString * nField;
@property (strong,nonatomic) NSString * pField;
@property (strong,nonatomic) NSString * kField;
@property (strong,nonatomic) NSString * micro;
@property (strong,nonatomic) NSString * density;
@property (strong,nonatomic) NSString * mg;
@property (strong,nonatomic) NSString * company;
@property (strong,nonatomic) NSString * categoryName;
@property (strong,nonatomic) NSString * categoryType;
@property (strong,nonatomic) NSString * description;
@property (strong,nonatomic) NSString * nitrogen;
@property (strong,nonatomic) NSString * amount;
@property (strong,nonatomic) NSString * waterAmount;
@property (strong,nonatomic) NSString * fertAmount;
@property (strong,nonatomic) NSString * growthStage;
@property (strong,nonatomic) NSString * created;
@property (strong,nonatomic) NSString * updated;



@end
