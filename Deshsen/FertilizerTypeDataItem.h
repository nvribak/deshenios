//
//  FertilizerTypeDataItem.h
//  Deshsen
//
//  Created by Avi Osipov on /2/614.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FertilizerTypeDataItem : NSObject

@property (strong,nonatomic) NSString * name;
@property (strong,nonatomic) NSString * type;

@end
