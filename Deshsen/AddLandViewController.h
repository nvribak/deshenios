//
//  AddLandViewController.h
//  Deshsen
//
//  Created by Avi Osipov on /18/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "KeyboardSupportViewController.h"
#import "MBProgressHUD.h"
#import "UIDropDownMenu.h"
#import "SearchCellDataItem.h"
#import "FertilizerViewController.h"
#import "CropDataManager.h"
#import "SpecieDataManager.h"
#import "LandDataItem.h"
#import "CameraViewController.h"
#import "GodzillaFileItem.h"
#import "TinyHelpers.h"
#import "LandDataManager.h"

@interface AddLandViewController : KeyboardSupportViewController <UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource , UIPickerViewDelegate , UIPickerViewDataSource,UIDropDownMenuDelegate,UIAlertViewDelegate> {
    
    UIPickerView *pickerView ;
    NSMutableArray *searchMenu ;
    int selectedTableRow ;
    NSString *cropOrSpecieOrStructure;
    
    NSArray * speciesArray;
    NSArray * cropsArray;
    NSArray * structuresArray;
    
    CropDataItem * selectedCrop;
    SpecieDataItem * selectedSpecie;
    NSString * selectedStructure;


    IBOutlet UIButton *structureLabel;

    IBOutlet UIButton *cropLabel;
    
    IBOutlet UIButton *specieLabel;
    
    CropDataManager * cropManager;
    SpecieDataManager * specieManager;
    LandDataItem *  newLandItem;
    
    
}

@property (nonatomic,strong) UIImage * landImage;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) LandDataItem * land;
@property (strong, nonatomic) LandDataItem * tempLand;
- (IBAction)addPhotoToCrop:(id)sender;
- (IBAction)saveNewLand:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) IBOutlet UIPickerView *picker;
- (IBAction)pickKindBtn:(id)sender;
- (IBAction)pickCropButton:(id)sender;
- (IBAction)pickStructureBtn:(id)sender;
@property (nonatomic) BOOL cameFromFarm;

@property (strong, nonatomic) IBOutlet UIImageView *landPreviewImage;
- (IBAction)enlargeImage:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *photoSubView;
@property (strong, nonatomic) IBOutlet UIImageView *largePhoto;
- (IBAction)closeSubView:(id)sender;

@end
