//
//  specieGrowthStagesDataItem.h
//  Deshsen
//
//  Created by Avi Osipov on /30/614.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface specieGrowthStagesDataItem : NSObject

@property (strong,nonatomic) NSString * _id;
@property (strong,nonatomic) NSString * specieId;
@property (strong,nonatomic) NSString * fromDay;
@property (strong,nonatomic) NSString * toDay;
@property (strong,nonatomic) NSString * stageName;

@end
