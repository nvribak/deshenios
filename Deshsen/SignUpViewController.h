//
//  SignUpViewController.h
//  Deshsen
//
//  Created by Avi Osipov on /17/714.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "KeyboardSupportViewController.h"
#import "TinyHelpers.h"

@interface SignUpViewController : KeyboardSupportViewController <UITextFieldDelegate>

- (IBAction)signUpBtn:(id)sender;

- (IBAction)cancelBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *emailField;
@property (strong, nonatomic) IBOutlet UITextField *passField;

@property (strong, nonatomic) IBOutlet UITextField *nameField;

@property (strong, nonatomic) IBOutlet UITextField *phoneField;


@end
