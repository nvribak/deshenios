//
//  FarmDataItem.h
//  Deshsen
//
//  Created by Avi Osipov on /6/414.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserDataItem.h"
@interface FarmDataItem : NSObject
@property (strong,nonatomic) NSString * _id;
@property (strong,nonatomic) NSString * autoNumber;
@property (strong,nonatomic) NSString * name;
@property (strong,nonatomic) NSString * address;
@property (strong,nonatomic) NSString * lat;
@property (strong,nonatomic) NSString * lng;
@property (strong,nonatomic) NSString * phone;
@property (strong,nonatomic) NSString * userId;
@property (strong,nonatomic) NSString * photo;


@end
