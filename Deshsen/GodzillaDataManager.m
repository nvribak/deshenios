//
//  GodzillaDataManager.m
//  Deshsen
//
//  Created by Avi Osipov on /16/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#define kServerAddress @"http://godzilla.appli.co.il/services/"
#define kServerAddress2 @"http://deshen.appli.co.il/services/"
#define kAppID @"123579593e9a4c56a39lkee53d9b434f"
#define autoincrement @"autoincrement=1"

#import "GodzillaDataManager.h"

@implementation GodzillaDataManager

- (id) init
{
    self = [super init];
    
    if (self != nil)
    {
        /// init stuff here
        
        NSLog(@"Godzilla Data Manager ready") ;
        self.secure = YES ;
        //  self.serverAddress = @"http://godzilla.appli.co.il/services/";
        
        /// check for internet connection
        
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        //  int remoteHostStatus = [reachability currentReachabilityStatus];
        
        
        //   NSLog(@"connection : %d" , remoteHostStatus ) ;
        
        //        if (remoteHostStatus == NotReachable) {
        //          NSLog(@"no internet connection found") ;
        //    }
        
    }
    
    return self;
}


- (id) initWithAutoIncrement: (BOOL) value
{
    self = [super init];
    
    if (self != nil)
    {
        /// init stuff here
        
        NSLog(@"Godzilla Data Manager ready") ;
        self.secure = YES ;
        self.autoIncrement = YES;
        //  self.serverAddress = @"http://godzilla.appli.co.il/services/";
        
        /// check for internet connection
        
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        //  int remoteHostStatus = [reachability currentReachabilityStatus];
        
        
        //   NSLog(@"connection : %d" , remoteHostStatus ) ;
        
        //        if (remoteHostStatus == NotReachable) {
        //          NSLog(@"no internet connection found") ;
        //    }
        
    }
    
    return self;
}


- (GodzillaFileItem *) uploadImage : (UIImage *) image withTitle : (NSString *) title
{
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    // RP: Empaquetando datos
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    [_params setObject:title forKey:@"title"];
    
    // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
    NSString *BoundaryConstant = @"V2ymHFg03ehbqgZCaKO6jy";
    
    // string constant for the post parameter 'file'
    NSString *FileParamConstant = @"userfile";
    
    //RP: Configurando la dirección
    NSURL *requestURL = [[NSURL alloc] initWithString:@"http://godzilla.appli.co.il/services/upload_file"];
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // add image data
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);;
    if (imageData) {
        printf("appending image data\n");
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\'%@\'; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%d", [body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    NSURLResponse *response = nil;
    NSError *err = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    NSString *str = [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];
    NSLog(@"Response : %@",str);
    
    
    
    
    //// parse response and reutrn
    
    GodzillaFileItem *fileInfo = [[GodzillaFileItem alloc ] init ] ;
    
    id jsonObj = [NSJSONSerialization JSONObjectWithData:[str dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    
    
    NSDictionary *item = [[ NSDictionary alloc] init ] ;
    item = [jsonObj objectForKey:@"data" ]  ;
    
    
    fileInfo._id = [item objectForKey:@"_id"] ;
    fileInfo.url = [item objectForKey:@"url"] ;
    fileInfo.title = [item objectForKey:@"title"] ;
    
    
    
    return fileInfo ;
    
    
}




- (NSString*)sendServerRequest:(NSString*) command withGetQuery : (NSString *) getQuery withPostQuery:(NSString*)postQuery
{
    
    NSString *requestURL = [kServerAddress stringByAppendingString:command] ;
    
    
    
    if (([getQuery isEqualToString:@""])&&(self.autoIncrement==YES)&&(self.secure==YES))
        requestURL = [requestURL stringByAppendingFormat:@"?api_key=%@&%@&%@", kAppID ,autoincrement, getQuery  ] ;
    else if (self.secure == YES)
        requestURL = [requestURL stringByAppendingFormat:@"?api_key=%@&%@", kAppID , getQuery  ] ;
        else
            requestURL = [requestURL stringByAppendingFormat:@"?%@", getQuery  ] ; // access without api key

    
    NSLog(@"GZ Request : %@" , requestURL ) ;
    NSLog(@"GZ Post Query : %@" , postQuery ) ;
    
    NSString *res = nil;
    NSURL *url = [ NSURL URLWithString:[ requestURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] ;
    
    NSData *postData = [postQuery dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    
    
    NSHTTPURLResponse* urlResponse = nil;
    NSError* error = nil;
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
    
    NSLog(@"GZ Server Response Status : %d", [urlResponse statusCode]);
    
    if ([urlResponse statusCode] >= 200 && [urlResponse statusCode] < 300 && response){
        res = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
        
        NSLog(@"GZ Server Response : %@", res);
    }
    
    
    
    return res;
    
    
    
    
}


- (NSString*)sendServerRequest2:(NSString*) command withGetQuery : (NSString *) getQuery withPostQuery:(NSString*)postQuery
{
    
    NSString *requestURL = [kServerAddress2 stringByAppendingString:command] ;
    
    
    
  //  if (([getQuery isEqualToString:@""])&&(self.autoIncrement==YES)&&(self.secure==YES))
  //      requestURL = [requestURL stringByAppendingFormat:@"?api_key=%@&%@&%@", kAppID ,autoincrement, getQuery  ] ;
 //   else if (self.secure == YES)
  //      requestURL = [requestURL stringByAppendingFormat:@"?api_key=%@&%@", kAppID , getQuery  ] ;
 //   else
    //    requestURL = [requestURL stringByAppendingFormat:@"?%@", getQuery  ] ; // access without api key
    
    
    NSLog(@"GZ Request : %@" , requestURL ) ;
    NSLog(@"GZ Post Query : %@" , postQuery ) ;
    
    NSString *res = nil;
    NSURL *url = [ NSURL URLWithString:[ requestURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] ;
    
    NSData *postData = [postQuery dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    
    
    NSHTTPURLResponse* urlResponse = nil;
    NSError* error = nil;
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
    
    NSLog(@"GZ Server Response Status : %d", [urlResponse statusCode]);
    
    if ([urlResponse statusCode] >= 200 && [urlResponse statusCode] < 300 && response){
        res = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
        
        NSLog(@"GZ Server Response : %@", res);
    }
    
    
    
    return res;
    
    
    
    
}


+(BOOL)sendMail:(Email *)email
{
    
    GodzillaDataManager *manager = [[GodzillaDataManager alloc] init] ;
    
    NSString *getQuery = [NSString stringWithFormat:@"to_email=%@&to_name=%@&responder_name=%@" ,
                          email.emailTo,@"מנהל מערכת",@"contact_deshen_3"] ;
    
    NSString *postRequest = [NSString stringWithFormat:@"user=%@&message=%@&farm=%@&land=%@&fertilizer=%@",
                             email.userDetails,email.message,email.farmDetails,email.landDetails,email.fertilizersDetails ] ;
    
    NSString *response = [ manager sendServerRequest:@"mail" withGetQuery:getQuery withPostQuery:postRequest] ;
    
    
    
    
    /// parse json response
    
    id jsonObj = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    
    
    
    
    NSString * success = [jsonObj objectForKey:@"success"];
    
    if (success)
    {
        return YES;
    }
    
    return NO;
    
    
    
    
}






@end

