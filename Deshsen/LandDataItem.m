//
//  LandDataItem.m
//  Deshsen
//
//  Created by Avi Osipov on /6/414.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "LandDataItem.h"

@implementation LandDataItem


/*
 @property (strong,nonatomic) NSString * _id;
 @property (strong,nonatomic) NSString * autoNumber;
 @property (strong,nonatomic) NSString * details;
 @property (strong,nonatomic) NSString * structure;
 @property (nonatomic) BOOL  status;
 @property (strong,nonatomic) NSString * farmId;
 @property (strong,nonatomic) NSString * photo;
 @property (strong,nonatomic) NSString * assigned_date;
 @property (strong,nonatomic) SpecieDataItem * specieItem;
 @property (strong,nonatomic) CropDataItem * cropItem;
 */


- (id)init {
    self = [super init];
    if (self) {
        [self initHelper];
    }
    return self;
}

- (void) initHelper {
    // Custom initialization
    self._id = @"0";
    self.autoNumber = @"0";
    self.details =@"מידע לא זמין";
    self.structure = @"מידע לא זמין";
    self.status = NO;
    self.farmId = @"0";
    self.photo = @"מידע לא זמין";
    self.assigned_date = @"מידע לא זמין";
    self.specieItem = [[SpecieDataItem alloc]init];
    self.cropItem = [[CropDataItem alloc]init];

    
}
@end
