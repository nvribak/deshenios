//
//  Email.h
//  Fair
//
//  Created by Avi Osipov on /10/414.
//  Copyright (c) 2014 Appli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Email : NSObject

@property (nonatomic, retain) NSString *emailTo;
@property (nonatomic, retain) NSString *message;
@property (nonatomic, retain) NSString *userDetails;
@property (nonatomic, retain) NSString *farmDetails;
@property (nonatomic, retain) NSString *landDetails;
@property (nonatomic, retain) NSString *fertilizersDetails;


@end
