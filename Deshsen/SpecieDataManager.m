//
//  SpecieDataManager.m
//  Deshsen
//
//  Created by Avi Osipov on /7/414.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "SpecieDataManager.h"

@implementation SpecieDataManager

-(NSArray *)getSpeciesByCropId:(NSString *)cropId
{
    GodzillaDataManager * manager = [[GodzillaDataManager alloc]init];
   
    NSString * request = [NSString stringWithFormat:@"crop_id=%@"  , cropId] ;
  
    NSString *response = [manager sendServerRequest:@"get/species" withGetQuery:request withPostQuery:@""] ;
    
    
    
    
    /// parse json response
    
    id jsonObj = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    
    
    if ([jsonObj isKindOfClass:[NSDictionary class]]) {
        
        
        
        NSArray *resultArray = [jsonObj objectForKey:@"data"];
        NSMutableArray *responseArray = [[NSMutableArray alloc] init ] ;
        
        for (NSDictionary *item in resultArray) {
            
            
            SpecieDataItem *specieItem = [[SpecieDataItem alloc] init] ;
            
            
            specieItem._id = [item objectForKey:@"_id"] ;
            specieItem.name = [item objectForKey:@"name"];
            
            [responseArray addObject:specieItem ] ;
            
            
            
        }
        
        
        
        
        return responseArray ;
    }
    return nil;
}

+(NSArray *)getSpeciesGrowthStagesTable
{
    GodzillaDataManager * manager = [[GodzillaDataManager alloc]init];
    
 //   NSString * request = [NSString stringWithFormat:@"crop_id=%@"  , cropId] ;
    
    NSString *response = [manager sendServerRequest:@"get/species_growth_stages" withGetQuery:@"" withPostQuery:@""] ;
    
    
    
    
    /// parse json response
    
    id jsonObj = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    
    
    if ([jsonObj isKindOfClass:[NSDictionary class]]) {
        
        
        
        NSArray *resultArray = [jsonObj objectForKey:@"data"];
        NSMutableArray *responseArray = [[NSMutableArray alloc] init ] ;
        
        for (NSDictionary *item in resultArray) {
            
            
            specieGrowthStagesDataItem *specieItem = [[specieGrowthStagesDataItem alloc] init] ;
            
            
            specieItem._id = [item objectForKey:@"_id"] ;
            specieItem.specieId = [item objectForKey:@"specie_id"];
            specieItem.fromDay = [item objectForKey:@"from_day"] ;
            specieItem.toDay = [item objectForKey:@"to_day"];
            specieItem.stageName = [item objectForKey:@"stage_name"] ;

            [responseArray addObject:specieItem ] ;
            
            
            
        }
        
        
        
        
        return responseArray ;
    }
    return nil;

}


@end
