//
//  EditFarmViewController.m
//  Deshsen
//
//  Created by Avi Osipov on /20/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "EditFarmViewController.h"
#define METERS_PER_MILE 1609.344

@interface EditFarmViewController ()

@end

@implementation EditFarmViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIColor *myColor = [UIColor colorWithRed:(185.0 / 255.0) green:(220.0 / 255.0) blue:(240.0 / 255.0) alpha: 1];
    self.subHeaderTitle.backgroundColor = myColor;
    self.mapView.delegate = self;
    [TinyHelpers addTextFieldPadding2:self.farmNameField ] ;
    [TinyHelpers addTextFieldPadding:self.phoneField ] ;
    [TinyHelpers addTextFieldPadding2:self.addressField ] ;
    manager = [[FarmDataManager alloc]init];
    
    
    
    //1
    /*
     CLLocationCoordinate2D zoomLocation;
     zoomLocation.latitude = 32.434973;
     zoomLocation.longitude= 34.920424;
     // 2
     MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
     
     // 3
     [_mapView setRegion:viewRegion animated:YES];
     
     */
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = @"חווה חדשה";
    [super setUpBarButton];
    
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [HUD setLabelText:NSLocalizedString(@"טוען נתונים...", @"טוען נתונים...")];
    [self.view addSubview:HUD];
    
    
    [HUD showWhileExecuting:@selector(_loadView ) onTarget:self withObject:nil animated:YES] ;
    
    
    
}

-(void)_loadView
{
    if (self.farmItem!=nil)
    {
        self.navigationItem.title = [NSString stringWithFormat:@"%@ - %@",self.farmItem.name,self.farmItem.autoNumber];
        self.farmNameField.text = self.farmItem.name;
        self.phoneField.text = self.farmItem.phone;
        self.addressField.text = self.farmItem.address;
        [self textFieldDidEndEditing:self.addressField];
        
    }
    
    /*
     @property (strong, nonatomic) IBOutlet UILabel *subHeaderTitle;
     @property (strong, nonatomic) IBOutlet UITextField *farmField;
     @property (strong, nonatomic) IBOutlet UITextField *phoneField;
     @property (strong, nonatomic) IBOutlet UITextField *mailField;
     @property (strong, nonatomic) IBOutlet MKMapView *mapView;
     @property (strong, nonatomic) FarmDataItem * farmItem;
     
     */
    
    
}

/*
 -(void)_doGoBack
 {
 [super clearBarButtons];
 [self.navigationController popViewControllerAnimated:YES];
 
 
 }
 */

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag == 3)
    {
        CLLocationCoordinate2D zoomLocation = [super geoCodeUsingAddress:textField.text];
        //   CLLocationCoordinate2D zoomLocation = _geoCodeUsingAddress(textField.text);
        MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, METERS_PER_MILE/2, METERS_PER_MILE/2);
        
        [_mapView setRegion:viewRegion animated:YES];
        point = [[MKPointAnnotation alloc] init];
        point.coordinate = zoomLocation;
        point.title = @"Where am I?";
        point.subtitle = @"I'm here!!!";
        
        [self.mapView addAnnotation:point];
        
    }
    
}



/*
 - (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
 {
 MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
 [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
 
 // Add an annotation
 MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
 point.coordinate = userLocation.coordinate;
 point.title = @"Where am I?";
 point.subtitle = @"I'm here!!!";
 
 [self.mapView addAnnotation:point];
 }
 
 */


- (IBAction)saveBtn:(id)sender {
    
    BOOL isValid = YES;
    self.phoneField.text = [self.phoneField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.addressField.text = [self.addressField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.farmNameField.text = [self.farmNameField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([self.phoneField.text isEqualToString:@""])
    {
        isValid = NO;
        [TinyHelpers showAlert:@"שדה חסר" withMessage:@"עליך להזין מספר טלפון"];
    }
    if ([self.addressField.text isEqualToString:@""])
    {
        isValid = NO;
        [TinyHelpers showAlert:@"שדה חסר" withMessage:@"עליך להזין כתובת"];
    }
    if ([self.farmNameField.text isEqualToString:@""])
    {
        isValid = NO;
        [TinyHelpers showAlert:@"שדה חסר" withMessage:@"עליך להזין שם חווה"];
    }
    if (isValid)
    {
        BOOL success;
        FarmDataItem * farm = [[FarmDataItem alloc]init];
        farm.name = self.farmNameField.text;
        farm.address = self.addressField.text;
        farm.lat = [NSString stringWithFormat:@"%f", point.coordinate.latitude];
        farm.lng = [NSString stringWithFormat:@"%f", point.coordinate.longitude];
        farm.userId = [UserDataManager getCurrentUser]._id;
        farm.phone = self.phoneField.text;
        if (self.updateFarm)
        {
            
            farm._id = self.farmItem._id;
            
             success = [manager updateFarm:farm];
            
            if (success)
            {
                
                UIAlertView *  customAlertView = [[UIAlertView alloc] initWithTitle:@"עדכון חווה"
                                                                            message:@"חווה עודכנה בהצלחה" delegate:nil
                                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [customAlertView show];
                
            }
            else
                [TinyHelpers showAlert:@"חווה חדשה" withMessage:@"לא ניתן לשמור את המידע"];

        }
        
        else
        {
  
             success =  [manager addFarm:farm];
            if (success)
            {
                
                UIAlertView *  customAlertView = [[UIAlertView alloc] initWithTitle:@"חווה חדשה"
                                                                            message:@"חווה הוספה בהצלחה" delegate:self
                                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [customAlertView show];
                
            }
            else
                [TinyHelpers showAlert:@"חווה חדשה" withMessage:@"לא ניתן לשמור את המידע"];

        }
        
    }
    
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    [self _doGoBack];
}
@end
