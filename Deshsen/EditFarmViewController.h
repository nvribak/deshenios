//
//  EditFarmViewController.h
//  Deshsen
//
//  Created by Avi Osipov on /20/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "KeyboardSupportViewController.h"
#import "TinyHelpers.h"
#import <MapKit/MapKit.h>
#import "FarmDataManager.h"
#import "FarmDataItem.h"
#import "MBProgressHUD.h"
#import "FarmViewController.h"

@interface EditFarmViewController : KeyboardSupportViewController <MKMapViewDelegate,MKAnnotation,CLLocationManagerDelegate,UIAlertViewDelegate>{
    FarmDataManager * manager;
    MKPointAnnotation *point;
    
}
@property (strong, nonatomic) IBOutlet UILabel *subHeaderTitle;
@property (strong, nonatomic) IBOutlet UITextField *phoneField;
@property (strong, nonatomic) IBOutlet UITextField *addressField;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) FarmDataItem * farmItem;
@property (strong, nonatomic) IBOutlet UITextField *farmNameField;
@property (nonatomic)  BOOL updateFarm;
- (IBAction)saveBtn:(id)sender;

@end
