//
//  ViewController.m
//  Deshsen
//
//  Created by Avi Osipov on /16/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view, typically from a nib.
  self.navigationItem.title = @"התחברות";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
