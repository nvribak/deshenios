//
//  LandDataManager.m
//  Deshsen
//
//  Created by Avi Osipov on /6/414.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "LandDataManager.h"

@implementation LandDataManager

static int idGen = 0;



-(NSArray *)getLandsByFarmId: (NSString *)farmId
{
    GodzillaDataManager * manager = [[GodzillaDataManager alloc]init];
    
    NSString *request = [NSString stringWithFormat:@"farm_id=%@"  , farmId] ;
    NSString *response = [manager sendServerRequest:@"map/lands" withGetQuery:request withPostQuery:@""] ;
    
    
    
    
    /// parse json response
    
    id jsonObj = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    
    
    if ([jsonObj isKindOfClass:[NSDictionary class]]) {
        
        
        
        NSArray *resultArray = [jsonObj objectForKey:@"data"];
        NSMutableArray *responseArray = [[NSMutableArray alloc] init ] ;
        
        for (NSDictionary *item in resultArray) {
            
            
            LandDataItem *landItem = [[LandDataItem alloc] init] ;
            
            
            
            
            landItem._id = [item objectForKey:@"_id"] ;
            landItem.autoNumber = [[item objectForKey:@"_auto_number"] stringValue];
            landItem.details = [item objectForKey:@"details"];
            landItem.structure = [item objectForKey:@"structure" ] ;
            landItem.status = [[item objectForKey:@"status"] boolValue];
            landItem.assigned_date = [item objectForKey:@"assigned_date"];
            landItem.photo = [item objectForKey:@"photo"];
            
            
         
            NSMutableArray *responseArray2 = [[NSMutableArray alloc] init ] ;
            responseArray2 = [item objectForKey:@"specie_id"];
            for (NSDictionary *item in responseArray2) {
                
                SpecieDataItem *specieItem = [[SpecieDataItem alloc] init] ;
                specieItem._id = [item objectForKey:@"_id"] ;
                specieItem.name = [item objectForKey:@"name"];

                
                landItem.specieItem = specieItem;
                
                
                NSMutableArray *responseArray3 = [[NSMutableArray alloc] init ] ;
                responseArray3 = [item objectForKey:@"crop_id"];
                for (NSDictionary *item in responseArray3) {
                    
                    CropDataItem *cropItem = [[CropDataItem alloc] init] ;
                    cropItem._id = [item objectForKey:@"_id"] ;
                    cropItem.name = [item objectForKey:@"name"];
                    
                    
                    landItem.cropItem = cropItem;
                }
                

                
            }
            
            
        
            [responseArray addObject:landItem ] ;
            
            
            
        }
        
        
        
        
        return responseArray ;
    }
    return nil;
}


-(LandDataItem *)getSingleLandById: (NSString *)landId
{
    GodzillaDataManager * manager = [[GodzillaDataManager alloc]init];
    
    NSString *request = [NSString stringWithFormat:@"_id=%@"  , landId] ;
    NSString *response = [manager sendServerRequest:@"map/lands" withGetQuery:request withPostQuery:@""] ;
    
    
    
    
    /// parse json response
    
    id jsonObj = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    
    
    if ([jsonObj isKindOfClass:[NSDictionary class]]) {
        
        
        
        NSArray *resultArray = [jsonObj objectForKey:@"data"];
        NSMutableArray *responseArray = [[NSMutableArray alloc] init ] ;
        
        for (NSDictionary *item in resultArray) {
            
            
            LandDataItem *landItem = [[LandDataItem alloc] init] ;
            
            
            
            
            landItem._id = [item objectForKey:@"_id"] ;
            landItem.autoNumber = [[item objectForKey:@"_auto_number"] stringValue];
            landItem.details = [item objectForKey:@"details"];
            landItem.structure = [item objectForKey:@"structure" ] ;
            landItem.status = [[item objectForKey:@"status"] boolValue];
            landItem.assigned_date = [item objectForKey:@"assigned_date"];
            landItem.photo = [item objectForKey:@"photo"];
            
            
            
            NSMutableArray *responseArray2 = [[NSMutableArray alloc] init ] ;
            responseArray2 = [item objectForKey:@"specie_id"];
            for (NSDictionary *item in responseArray2) {
                
                SpecieDataItem *specieItem = [[SpecieDataItem alloc] init] ;
                specieItem._id = [item objectForKey:@"_id"] ;
                specieItem.name = [item objectForKey:@"name"];
                
                
                landItem.specieItem = specieItem;
                
                
                NSMutableArray *responseArray3 = [[NSMutableArray alloc] init ] ;
                responseArray3 = [item objectForKey:@"crop_id"];
                for (NSDictionary *item in responseArray3) {
                    
                    CropDataItem *cropItem = [[CropDataItem alloc] init] ;
                    cropItem._id = [item objectForKey:@"_id"] ;
                    cropItem.name = [item objectForKey:@"name"];
                    
                    
                    landItem.cropItem = cropItem;
                }
                
                
                
            }
            
            
            
            [responseArray addObject:landItem ] ;
            
            
            
        }
        
        
        
        
        return [responseArray objectAtIndex:0] ;
    }
    return nil;
}

-(LandDataItem *)addLand:(LandDataItem *)land
{
    GodzillaDataManager *manager = [[GodzillaDataManager alloc] initWithAutoIncrement:YES] ;
    
    
    NSString *postQuery =@"";
    
    postQuery =   [ NSString stringWithFormat: @"&farm_id=%@&status=%hhd&structure=%@&specie_id=%@&assigned_date=%@&photo=%@"
                   ,land.farmId,land.status,land.structure,land.specieItem._id,land.assigned_date,land.photo] ;
    
    NSString * result =  [manager sendServerRequest:@"add/lands" withGetQuery:@"" withPostQuery:postQuery] ;
    
    /// parse json response
    
    id jsonObj = [NSJSONSerialization JSONObjectWithData:[result dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    
    
    NSLog(@"response : %@" , result ) ;
    NSString * land_id = [[jsonObj objectForKey:@"data"] objectForKey:@"_id"] ;
    
    return [self getSingleLandById:land_id];
    
    
    
    /// generate result array
    
    
    /*
    BOOL value =  [[dataObj objectForKey:@"ok"] boolValue]  ;
    if (value)
        return YES;
    return NO;
     */
}



- (BOOL) updateLand : (LandDataItem *) land
{
    
    
    
    GodzillaDataManager *manager = [[GodzillaDataManager alloc] init] ;
    
    NSString *postRequest = [NSString stringWithFormat:@"_id=%@&farm_id=%@&structure=%@&specie_id=%@&assigned_date=%@&photo=%@" ,
                             land._id,land.farmId,land.structure,land.specieItem._id,land.assigned_date,land.photo] ;
    
    NSString *result = [ manager sendServerRequest:@"update/lands" withGetQuery:@"" withPostQuery:postRequest] ;
    
    
    
    
    /// parse json response
    
    id jsonObj = [NSJSONSerialization JSONObjectWithData:[result dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    
    NSLog(@"response : %@" , result ) ;
    id dataObj = [jsonObj objectForKey:@"success"] ;
    
    /// generate result array
    
    
    
    BOOL value =  [[dataObj objectForKey:@"ok"] boolValue]  ;
    if (value)
        return YES;
    return NO;
}


@end
