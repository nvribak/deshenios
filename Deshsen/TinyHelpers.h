//
//  TinyHelpers.h
//  WXG
//
//  Created by Avi Osipov on /6/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TinyHelpers : NSObject
+ (void) showAlert : (NSString *) title withMessage : (NSString *) message ;
+ (NSDate*)convertMongoDate:(id) unixDate  ;
+ (NSString*)dateDiff:(NSDate*) date ;
+(NSString *)returnCurrentDate;
+(NSString *)fixDateFormat: (NSString *)date;
+(void) addTextFieldPadding:(UITextField*) textField;
+(void) addTextFieldPadding2:(UITextField*) textField;
+(NSString*) convertDateToString : (NSDate *)date;
+(NSString *)calculateDateDifferenceInWeeks :(NSString *)date;
+(NSString *)calculateDateDifferenceInWeeksFromDate :(NSString *)fromDate toDate :(NSString *)toDate;
+(NSString *)convertStringFromDate:(NSDate *)date;
@end
