//
//  FertilizerDataManager.h
//  Deshsen
//
//  Created by Avi Osipov on /7/414.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GodzillaDataManager.h"
#import "FertilizerDataItem.h"
#import "TinyHelpers.h"

@interface FertilizerDataManager : NSObject

-(NSArray *)getFertilizersByLandId: (NSString *)landId;
-(NSArray *)getFertilizers;
+(NSArray *)getMatchResultBySpecieID: (NSString *)specieID onWeek: (NSString *)week  atStructure: (NSString * )structure MatchToFertilizerType: (NSString *) fertilizerCompanyType;
-(BOOL)addFertilizer:(NSString *)fertilizerID toLand: (NSString *)landID withFertAmount : (NSString *)fertAmount andWaterAmount: (NSString *)waterAmount andSumResult :(NSString *)sum;
-(NSString *)getGrowthStageIdBySpecieId: (NSString *)specieID andWeek : (NSString *)week andStructure: (NSString *)structure;
-(NSArray *)getFertilizersByGrowthStageID: (NSString *)growthStageID;


@end
