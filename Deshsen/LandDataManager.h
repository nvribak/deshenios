//
//  LandDataManager.h
//  Deshsen
//
//  Created by Avi Osipov on /6/414.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GodzillaDataManager.h"
#import "LandDataItem.h"

@interface LandDataManager : NSObject


-(NSArray *)getLandsByFarmId: (NSString *)farmId;
-(LandDataItem *)addLand:(LandDataItem *)land;
- (BOOL) updateLand : (LandDataItem *) land;
-(LandDataItem *)getSingleLandById: (NSString *)landId;
@end
