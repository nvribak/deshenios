//
//  UserDataItem.h
//  Deshsen
//
//  Created by Avi Osipov on /16/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDataItem : NSObject

@property (strong,nonatomic) NSString *_id ;
@property (strong,nonatomic) NSString *email ;
@property (strong,nonatomic) NSString *fullName ;
@property (strong,nonatomic) NSString *password ;
@property (strong,nonatomic) NSString *phone ;




@end
