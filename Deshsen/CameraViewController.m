//
//  CameraViewController.m
//  Deshsen
//
//  Created by Avi Osipov on /10/414.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "CameraViewController.h"

@interface CameraViewController ()

@end

@implementation CameraViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    if (self.cameFromNewLand)
    {
        self.navigationItem.title = @"צלם חלקה";
        
        [self.takePhoto setTitle:@"צלם חלקה" forState:UIControlStateNormal];
        
    }
    
    
    else
        
    {
        
        manager = [[GodzillaDataManager alloc]init];
        farmManager = [[FarmDataManager alloc]init];
        
        
        self.navigationItem.title = @"צלם חווה";
        
        
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            
            UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                  message:@"Device has no camera"
                                                                 delegate:nil
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles: nil];
            
            [myAlertView show];
            
        }
        
    }
    
    
    
	// Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super setUpBarButton];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super clearBarButtons];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)takePhoto:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    
}
- (IBAction)selectPhoto:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    
    
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.imageView.image = chosenImage;
    
    
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [HUD setLabelText:NSLocalizedString(@"שומר תמונה", @"שומר תמונה")];
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(saveImage) onTarget:self withObject:nil animated:YES] ;
    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    if (!self.cameFromNewLand)
        [TinyHelpers showAlert:@"צלם חווה" withMessage:@"תמונה נשמרה בהצלחה"];
    
}

-(void)saveImage
{
    
    if (self.cameFromNewLand)
    {
        [[SingleProjectManager sharedProject] setPhotoImage:self.imageView.image];
    }
    
    else
    {
        GodzillaFileItem *fileInfo = [manager uploadImage:self.imageView.image withTitle:@"farm_name"];
        
        NSLog(@"new file url : %@" , fileInfo.url ) ;
        
        self.farm.photo = fileInfo.url;
        
        [farmManager updateFarmPhoto:self.farm];
        
    }
    
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}






@end
