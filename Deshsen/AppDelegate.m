//
//  AppDelegate.m
//  Deshsen
//
//  Created by Avi Osipov on /16/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
   // self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
    
   /*
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault] ;
    [[UINavigationBar appearance] setBackgroundColor:[UIColor colorWithRed:239.0/255.0 green:64.0/255.0 blue:55.0/255.0 alpha:1.0]];
    
    /// add image (logo) to nav-bar
    
    UIImage *image = [UIImage imageNamed:@"header_bg_2.png"];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.frame = CGRectMake( 160 - imageView.frame.size.width/2  ,
                                 22 - imageView.frame.size.height/2 ,
                                 imageView.frame.size.width,
                                 imageView.frame.size.height) ;
    
    [[UINavigationBar appearance] addSubview:imageView];
*/
    
   /*
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont fontWithName:@"Helvetica-Bold" size:16],
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
    [[UINavigationBar appearance] setTitleTextAttributes:textAttributes];
    
    */
    
    
    
    
    
    /*
    UIButton *butt=[UIButton buttonWithType:UIButtonTypeCustom ];
    [butt setFrame:CGRectMake(2, 2, 40, 40)];
    [butt setImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    
    [butt addTarget:self action:@selector(_backToPermitController) forControlEvents:UIControlEventTouchDown];
    [butt setTag:100];
    
    
    //  self.navigationItem.leftBarButtonItem = leftButton;
    [[UINavigationBar appearance] addSubview:butt];
    
    UIButton *butt2=[UIButton buttonWithType:UIButtonTypeCustom ];
    [butt2 setFrame:CGRectMake(44, 2, 40, 40)];
    [butt2 setImage:[UIImage imageNamed:@"settings_button.png"] forState:UIControlStateNormal];
    
    [butt2 addTarget:self action:@selector(_backToPermitController) forControlEvents:UIControlEventTouchDown];
    [butt2 setTag:200];
    
    
    //  self.navigationItem.leftBarButtonItem = leftButton;
    [[UINavigationBar appearance] addSubview:butt2];
    
    UIButton *butt3=[UIButton buttonWithType:UIButtonTypeCustom ];
    [butt3 setFrame:CGRectMake(278, 2, 40, 40)];
    [butt3 setImage:[UIImage imageNamed:@"search_button.png"] forState:UIControlStateNormal];
    
    [butt3 addTarget:self action:@selector(_backToPermitController) forControlEvents:UIControlEventTouchDown];
    [butt3 setTag:300];
    
    
    //  self.navigationItem.leftBarButtonItem = leftButton;
    [[UINavigationBar appearance] addSubview:butt3];
    
    */
    
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIFont fontWithName:@"Helvetica-Bold" size:20],NSFontAttributeName,
                                    [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
    [[UINavigationBar appearance] setTitleTextAttributes:textAttributes];
    
    
    
    
    
    UIImage *navBackgroundImage = [UIImage imageNamed:@"header_bg_2.png"];
    [[UINavigationBar appearance] setBackgroundImage:navBackgroundImage forBarMetrics:UIBarMetricsDefault];

  //  self.window.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
  //  [UIView appearance].backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
    
   
    
    // Change the appearance of other navigation button
  //  UIImage *barButtonImage = [[UIImage imageNamed:@"search_button.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 6, 0, 6)];
   // [[UIBarButtonItem appearance] setBackgroundImage:barButtonImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    // Override point for customization after application launch.
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
