//
//  FertilizerCell.h
//  Deshsen
//
//  Created by Avi Osipov on /20/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FertilizerCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *minusBtn;

@property (strong, nonatomic) IBOutlet UIButton *plusBtn;

@property (strong, nonatomic) IBOutlet UIButton *buyBtn;

@property (strong, nonatomic) IBOutlet UIButton *editBtn;
@property (strong, nonatomic) IBOutlet UILabel *fertilizerName;
@property (strong, nonatomic) IBOutlet UITextView *fertilizerTextView;

@end
