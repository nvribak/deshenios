//
//  SpecieDataManager.h
//  Deshsen
//
//  Created by Avi Osipov on /7/414.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GodzillaDataManager.h"
#import "SpecieDataItem.h"
#import "specieGrowthStagesDataItem.h"

@interface SpecieDataManager : NSObject

-(NSArray *)getSpeciesByCropId:(NSString *)cropId;

+(NSArray *)getSpeciesGrowthStagesTable;
@end
