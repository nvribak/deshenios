//
//  GodzillaDataManager.h
//  Deshsen
//
//  Created by Avi Osipov on /16/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "Reachability.h"
#import "GodzillaFileItem.h"
#import "Email.h"
@interface GodzillaDataManager : NSObject


- (id) initWithAutoIncrement: (BOOL) value;

- (NSString*)sendServerRequest:(NSString*) command withGetQuery : (NSString *) getQuery withPostQuery:(NSString*)postQuery ;
- (NSString*)sendServerRequest2:(NSString*) command withGetQuery : (NSString *) getQuery withPostQuery:(NSString*)postQuery;

@property (nonatomic,assign) BOOL secure ;
@property (nonatomic,assign) BOOL autoIncrement ;




- (GodzillaFileItem *) uploadImage : (UIImage *) image withTitle : (NSString *) title ;
+(BOOL)sendMail:(Email *)email;

@end
