//
//  KeyboardSupportViewController.m
//  Deshsen
//
//  Created by Avi Osipov on /16/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "KeyboardSupportViewController.h"

@interface KeyboardSupportViewController ()

@end

@implementation KeyboardSupportViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated ];
	
    
    /// add tap recognizer support
    
    
    [self setDeleageToAllTextFields] ;
    
    
}




- (void) setDeleageToAllTextFields
{
    
    for(UIView *subview in self.view.subviews)
    {
        
        
        /// if we have view inside view then check its sub-views
        
        if([subview isKindOfClass: [UIView class]])
        {
            
            
            for(UIView *view in subview.subviews)
            {
                
                if([view isKindOfClass: [UITextView class]])
                {
                    ((UITextView*)view).delegate = (id) self;
                }
                
                if([view isKindOfClass: [UITextField class]])
                {
                    ((UITextField*)view).delegate = (id) self;
                }
                
                
            }
            
            
        }
        
        //////////////////////////////////////////////////////////
        
        
        if([subview isKindOfClass: [UITextView class]])
        {
            ((UITextView*)subview).delegate = (id) self;
        }
        
        if([subview isKindOfClass: [UITextField class]])
        {
            ((UITextField*)subview).delegate = (id) self;
        }
    }
    
    
    
    
}


/// hide on touch

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}


/// hide on return

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setUpBarButton
{
    self.navigationItem.hidesBackButton = YES;
    
    
    
    UIButton *butt=[UIButton buttonWithType:UIButtonTypeCustom ];
    [butt setFrame:CGRectMake(2, 2, 40, 40)];
    [butt setImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    
    [butt addTarget:self action:@selector(_doGoBack) forControlEvents:UIControlEventTouchDown];
    [butt setTag:100];
    
    
    //  self.navigationItem.leftBarButtonItem = leftButton;
    [self.navigationController.navigationBar addSubview:butt];
    
    UIButton *butt2=[UIButton buttonWithType:UIButtonTypeCustom ];
    [butt2 setFrame:CGRectMake(278, 2, 40, 40)];
    [butt2 setImage:[UIImage imageNamed:@"settings_button.png"] forState:UIControlStateNormal];
    
    [butt2 addTarget:self action:@selector(_doOpenSettings) forControlEvents:UIControlEventTouchDown];
    [butt2 setTag:200];
    
    
    //  self.navigationItem.leftBarButtonItem = leftButton;
    [self.navigationController.navigationBar addSubview:butt2];
  /*
    UIButton *butt3=[UIButton buttonWithType:UIButtonTypeCustom ];
    [butt3 setFrame:CGRectMake(278, 2, 40, 40)];
    [butt3 setImage:[UIImage imageNamed:@"search_button.png"] forState:UIControlStateNormal];
    
    [butt3 addTarget:self action:@selector(_doOpenSearch) forControlEvents:UIControlEventTouchDown];
    [butt3 setTag:300];
    
    
    //  self.navigationItem.leftBarButtonItem = leftButton;
    [self.navigationController.navigationBar addSubview:butt3];
   */
}

-(void)clearBarButtons
{
    for (UIView *subView in self.navigationController.navigationBar.subviews)
    {
        if (subView.tag == 100 || subView.tag ==200 || subView.tag == 300)

        {
            [subView removeFromSuperview];
        }
    }

}

-(void)_doGoBack
{
    [self clearBarButtons];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)_doOpenSettings
{
    [self clearBarButtons];
    EditAccountViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"EditAccount"];
    [controller setUser:[UserDataManager getCurrentUser]];
    [self.navigationController pushViewController:controller animated:YES];
    
}

- (CLLocationCoordinate2D) geoCodeUsingAddress:(NSString *)address
{
    
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }

    CLLocationCoordinate2D center;
    center.latitude = latitude;
    center.longitude = longitude;
    return center;

}



- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text
{
    
    if ([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
        // Return FALSE so that the final '\n' character doesn't get added
        return NO;
    }
    // For any other character return TRUE so that the text gets added to the view
    return YES;
}

@end

