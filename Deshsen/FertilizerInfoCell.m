//
//  FertilizerInfoCell.m
//  Deshsen
//
//  Created by Avi Osipov on /20/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "FertilizerInfoCell.h"

@implementation FertilizerInfoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
