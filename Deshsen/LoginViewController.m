//
//  LoginViewController.m
//  Deshsen
//
//  Created by Avi Osipov on /16/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    manager = [[UserDataManager alloc]init];
    self.navigationItem.title = @"התחברות";
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIFont fontWithName:@"Helvetica-Bold" size:20],NSFontAttributeName,
                                    [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
    [[UINavigationBar appearance] setTitleTextAttributes:textAttributes];
    
    
    UIImage *navBackgroundImage = [UIImage imageNamed:@"header_bg_2.png"];
    [[UINavigationBar appearance] setBackgroundImage:navBackgroundImage forBarMetrics:UIBarMetricsDefault];
    [TinyHelpers addTextFieldPadding:self.usernameField ] ;
    [TinyHelpers addTextFieldPadding:self.passwordField ] ;

}
-(void)viewWillAppear:(BOOL)animated
{
    
    if ([UserDataManager getCurrentUser]!=nil)
    {
        [self dismissViewControllerAnimated:YES completion:nil];

    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)forgotPassBtn:(id)sender {
}



- (IBAction)loginBtn:(id)sender {
    if ([self.usernameField.text isEqualToString:@""] || [self.passwordField.text isEqualToString:@""])
        [TinyHelpers showAlert:@"כניסה" withMessage:@"ישנם שדות חסרים. עליך למלא את השדות במלואם."];
    else
    {
       BOOL login =  [UserDataManager login:self.usernameField.text Password:self.passwordField.text];
        if (login)
        {
            [TinyHelpers showAlert:@"כניסה" withMessage:@"ברוך הבא"] ;
            [self dismissViewControllerAnimated:YES completion:nil];

        }
        else
        {
            [TinyHelpers showAlert:@"כניסה למערכת" withMessage:@"שם המשתמש או הסיסמה שגויים. נא הזן פרטים שנית."];

        }
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField.tag == 2)
        [self animateTextField: textField up: YES];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag == 2)
        [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 80; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (IBAction)facebookBtn:(id)sender {
}

- (IBAction)openSignUpBtn:(id)sender {
    SignUpViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUp"];
 //   [self.navigationController pushViewController:controller animated:YES];
    [self presentViewController:controller animated:YES completion:nil];

    

}




    
    
    
    
    

@end
