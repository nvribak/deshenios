//
//  MainViewController.h
//  Deshsen
//
//  Created by Avi Osipov on /16/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "KeyboardSupportViewController.h"
#import "UserDataManager.h"
#import "LoginViewController.h"
#import "MBProgressHUD.h"
#import "FarmCell.h"
#import "FarmViewController.h"
#import "EditAccountViewController.h"
#import "FarmDataManager.h"
#import "FarmDataItem.h"

@interface MainViewController : KeyboardSupportViewController <UITableViewDataSource,UITableViewDelegate>{
    UserDataItem * user;
    FarmDataManager * manager;
    NSArray * farmsArray;
    
}
@property (strong, nonatomic) IBOutlet UILabel *usernameField;
- (IBAction)addFarmBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
