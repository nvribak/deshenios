//
//  FarmDataItem.m
//  Deshsen
//
//  Created by Avi Osipov on /6/414.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "FarmDataItem.h"

@implementation FarmDataItem

- (id)init {
    self = [super init];
    if (self) {
        [self initHelper];
    }
    return self;
}

- (void) initHelper {
    // Custom initialization
    self._id = @"0";
    self.autoNumber = @"0";
    self.name =@"מידע לא זמין";
    self.address = @"מידע לא זמין";
    self.lat = @"0";
    self.lng = @"0";
    self.photo = @"מידע לא זמין";
    self.userId = @"0";
    
    /*
     
     @property (strong,nonatomic) NSString * _id;
     @property (strong,nonatomic) NSString * autoNumber;
     @property (strong,nonatomic) NSString * name;
     @property (strong,nonatomic) NSString * address;
     @property (strong,nonatomic) NSString * lat;
     @property (strong,nonatomic) NSString * lng;
     @property (strong,nonatomic) NSString * phone;
     @property (strong,nonatomic) NSString * userId;
     @property (strong,nonatomic) NSString * photo;
     */
}

@end
