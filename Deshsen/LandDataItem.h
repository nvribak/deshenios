//
//  LandDataItem.h
//  Deshsen
//
//  Created by Avi Osipov on /6/414.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CropDataItem.h"
#import "SpecieDataItem.h"

@interface LandDataItem : NSObject
@property (strong,nonatomic) NSString * _id;
@property (strong,nonatomic) NSString * autoNumber;
@property (strong,nonatomic) NSString * details;
@property (strong,nonatomic) NSString * structure;
@property (nonatomic) BOOL  status;
@property (strong,nonatomic) NSString * farmId;
@property (strong,nonatomic) NSString * photo;
@property (strong,nonatomic) NSString * assigned_date;
@property (strong,nonatomic) SpecieDataItem * specieItem;
@property (strong,nonatomic) CropDataItem * cropItem;


@end
