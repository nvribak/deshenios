//
//  EditAccountViewController.h
//  Deshsen
//
//  Created by Avi Osipov on /20/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

//#import "KeyboardSupportViewController.h"
#import "TinyHelpers.h"
#import "UserDataManager.h"
#import "UserDataItem.h"
@interface EditAccountViewController : UIViewController <UITextFieldDelegate>{
    UserDataManager * manager;
    
}
@property (strong,nonatomic) UserDataItem * user;
@property (strong, nonatomic) IBOutlet UITextField *fullNameField;
@property (strong, nonatomic) IBOutlet UITextField *addressField;
@property (strong, nonatomic) IBOutlet UITextField *phoneField;
@property (strong, nonatomic) IBOutlet UITextField *emailField;

- (IBAction)saveChangesBtn:(id)sender;


@end
