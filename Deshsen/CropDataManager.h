//
//  CropDataManager.h
//  Deshsen
//
//  Created by Avi Osipov on /7/414.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GodzillaDataManager.h"
#import "CropDataItem.h"

@interface CropDataManager : NSObject

-(NSArray *)getAllCrops: (NSString *)cropId;

@end
