//
//  FertilizerTypeDataItem.m
//  Deshsen
//
//  Created by Avi Osipov on /2/614.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "FertilizerTypeDataItem.h"

@implementation FertilizerTypeDataItem

- (BOOL)isEqual:(id)other {
    if (other == self)
        return YES;
    if (!other || ![other isKindOfClass:[self class]])
        return NO;
    return [self isEqualToItem:other];
}

- (BOOL)isEqualToItem:(FertilizerTypeDataItem *)item{
    if (self == item)
        return YES;
    if (![(id)[self name] isEqual:[item name]])
        return NO;
    if (![[self type] isEqual:[item type]])
        return NO;
    return YES;
}

@end
