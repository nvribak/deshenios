//
//  FertilizerViewController.m
//  Deshsen
//
//  Created by Avi Osipov on /19/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#define emailAddress @"omrikaye@gmail.com"
#define testEmailAddress @"ran@appli.co.il"

#import "FertilizerViewController.h"

@interface FertilizerViewController ()

@end

@implementation FertilizerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]]];
    self.scrollView.frame = CGRectMake(0, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height);
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, 490);
    self.scrollView.delegate = self;
    fertilizersArray = [[NSArray alloc]init];
    fertilizerAmountArray = [[NSMutableArray alloc]init];
    fertilizerWaterArray = [[NSMutableArray alloc]init];
    
    manager = [[FertilizerDataManager alloc]init];
    
    MatchResultFertilizers = [[NSArray alloc]init];
    
    
    
    fertilizerType = [[NSMutableSet alloc]init];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    selectedIndex = -1;
    
    [super setUpBarButton];
    
    
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [HUD setLabelText:NSLocalizedString(@"טוען נתונים...", @"טוען נתונים...")];
    [self.view addSubview:HUD];
    
    
    [HUD showWhileExecuting:@selector(_loadView ) onTarget:self withObject:nil animated:YES] ;
    
    
    
    
}

-(void)_loadView
{
    
    self.navigationItem.title = [NSString stringWithFormat:@"חלקת %@ - %@",self.landItem.specieItem.name,self.landItem.autoNumber];
    
    
    /// set first picker array (takes the fertilizers company_type fields and presents only the company_type)
    
    sortedFertilizerTypesArray = [[NSMutableArray alloc]init];
    
    NSString * growthStageID = [manager getGrowthStageIdBySpecieId:self.landItem.specieItem._id andWeek:[TinyHelpers calculateDateDifferenceInWeeks:self.landItem.assigned_date] andStructure:self.landItem.structure];
    
  
    
    
    
    fertilizersArray = [manager getFertilizersByGrowthStageID:growthStageID];
    
    for (FertilizerDataItem * fert in fertilizersArray) {
        
        FertilizerTypeDataItem * item = [[FertilizerTypeDataItem alloc]init];
        item.name = fert.categoryName;
        item.type = fert.categoryType;
        if (![sortedFertilizerTypesArray containsObject:item])
            [sortedFertilizerTypesArray addObject:item];
        
    }
    
    
    // sortedFertilizerTypesArray = [fertilizerType allObjects];
    
    //  sortedFertilizerTypesArray = [sortedFertilizerTypesArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    
    /// set second picker array (ferilizer amount by sacks)
    
    
    for (int i = 1; i<=25; i++) {
        [fertilizerAmountArray addObject:[NSString stringWithFormat:@"%d",i]];
        
    }
    
    
    int i =100;
    
    while (i<=10000) {
        [fertilizerWaterArray addObject:[NSString stringWithFormat:@"%d",i]];
        i+=100;
        
    }
    /// set third picker array (water amount)
    
    
    
    /// initialize first picker index (to present default fertilizers to user )
    
    //sets first picker
    
    if ([sortedFertilizerTypesArray count]>0)
        
    {
    
    fertType =  ((FertilizerTypeDataItem*)[sortedFertilizerTypesArray objectAtIndex:0]).name;
    [self.fertilizerLabel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.fertilizerLabel setTitle:fertType forState:UIControlStateNormal];
    fertilierAmount = nil;
    waterAmount = nil;
    [self.fertAmountLabel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.fertAmountLabel setTitle: @"בחר כמות" forState:UIControlStateNormal];
    [self.waterAmountLabel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.waterAmountLabel setTitle:@"בחר כמות" forState:UIControlStateNormal];
    
    
    // sets second and third pickers
    
    if ([((FertilizerTypeDataItem*)[sortedFertilizerTypesArray objectAtIndex:0]).type isEqualToString:@"נוזלי"])
    {
        
        [self.fertAmountLabel setEnabled:NO];
        [self.waterAmountLabel setEnabled:NO];
        [self.fertAmountLabel setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [self.waterAmountLabel setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        
        
    }
    else
    {
        fertilierAmount = [fertilizerAmountArray objectAtIndex:7];
        [self.fertAmountLabel setTitle: fertilierAmount forState:UIControlStateNormal];
        waterAmount = [fertilizerWaterArray objectAtIndex:14];
        [self.waterAmountLabel setTitle:waterAmount forState:UIControlStateNormal];
        
    }
    
    
    // load table view with data (with the following details: specie, week, fertType and structure)
    
    
    MatchResultFertilizers = [FertilizerDataManager getMatchResultBySpecieID:self.landItem.specieItem._id onWeek:[TinyHelpers calculateDateDifferenceInWeeks:self.landItem.assigned_date] atStructure:self.landItem.structure MatchToFertilizerType:fertType];
    
    
    
   
    
    // [self.tableView reloadData];
    /*
    if (![MatchResultFertilizers count])
    {
        [TinyHelpers showAlert:@"תוצאות דישון" withMessage:@"לא נמצאו דשנים מתאימים לנתוני החיפוש"];
        self.tableView.hidden = YES;
    }
     */
    [self reloadTableView];
    
  //  [self pickFertilizerBtn:nil];
    NSLog(@"fff");
    }
    
    else
    {
        [TinyHelpers showAlert:@"תוצאות דישון" withMessage:@"לא נמצאו דשנים מתאימים לנתוני החיפוש"];
        self.tableView.hidden = YES;
    }
    
    
    
}
/*
-(void)viewDidAppear:(BOOL)animated
{
     if ([MatchResultFertilizers count]==0)
     {
     [TinyHelpers showAlert:@"תוצאות דישון" withMessage:@"לא נמצאו דשנים מתאימים לנתוני החיפוש"];
     self.tableView.hidden = YES;
     }
    
}
*/

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (selectedIndex == indexPath.row)
        return 170;
    else
        return 33;
}

/** table view stuff goes here ***/


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [MatchResultFertilizers count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    FertilizerDataItem * fertilizer = [[FertilizerDataItem alloc]init];
    
    fertilizer = [MatchResultFertilizers objectAtIndex:indexPath.row];
    
    
    
    static NSString *simpleTableIdentifier = @"FertilizerCell";
    
    FertilizerCell *cell = (FertilizerCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FertilizerCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    
    cell.fertilizerName.text = fertilizer.name;
    
    
    ///set inner cell data calculation (when cell will be expanded)
    
    
    // if fertilizer is not liquidy
    
    
    // calclate the amount of fertilizer:
    
    if (([fertilizer.categoryType isEqualToString:@"מוצק"])&&(waterAmount!=nil)&&(fertilierAmount!=nil))
    {
        
        float denominator = 0.0;
        float numerator = 0.0;
        
        
        denominator = ([fertilierAmount floatValue]*25000*([fertilizer.nField floatValue]/100))/[waterAmount floatValue];
        numerator = [fertilizer.nitrogen floatValue];
        
        float sum = numerator/denominator;
        
        if (fertilizer.description!=nil)
        {
            
            cell.fertilizerTextView.text = [NSString stringWithFormat:@"%@\n\n%@ %.2f %@",fertilizer.description,@"יש לדשן",sum,@"ליטרים ליום מסוג הדשן."];
            
        }
        else
        {
            cell.fertilizerTextView.text = [NSString stringWithFormat:@"%@\n\n%@ %.2f %@",@"אין תיאור לדשן הנבחר.",@"יש לדשן",sum,@"ליטרים ליום מסוג הדשן."];
        }
        ((FertilizerDataItem *)[MatchResultFertilizers objectAtIndex:indexPath.row]).amount = [NSString stringWithFormat:@"%.2f",sum];
        
    }
    
    
    
    // else
    else
    {
        
        float denominator = 0.0;
        float numerator = 0.0;
        
        
    //    denominator = ([fertilierAmount floatValue]*25000*([fertilizer.nField floatValue]/100))/[waterAmount floatValue];
        denominator = [fertilizer.density floatValue]*1000*([fertilizer.nField floatValue]/100);

        numerator = [fertilizer.nitrogen floatValue];
        
        float sum = numerator/denominator;
        
        if (fertilizer.description!=nil)
        {
            
            cell.fertilizerTextView.text = [NSString stringWithFormat:@"%@\n\n%@ %.2f %@",fertilizer.description,@"יש לדשן",sum,@"ליטרים ליום מסוג הדשן."];
            
        }
        else
        {
            cell.fertilizerTextView.text = [NSString stringWithFormat:@"%@\n\n%@ %.2f %@",@"אין תיאור לדשן הנבחר.",@"יש לדשן",sum,@"ליטרים ליום מסוג הדשן."];
        }
        ((FertilizerDataItem *)[MatchResultFertilizers objectAtIndex:indexPath.row]).amount = [NSString stringWithFormat:@"%.2f",sum];
    }
    
    
    
    
    
    if (selectedIndex== indexPath.row)
    {
        
        //do expend cell stuff
        cell.minusBtn.hidden = NO;
        cell.plusBtn.hidden = YES;
        
        
        
        
    }
    
    else
    {
        cell.minusBtn.hidden = YES;
        cell.plusBtn.hidden = NO;
        //do close cell stuff
        
    }
    
    
    
    cell.backgroundView.backgroundColor = [UIColor clearColor];
    
    
    cell.editBtn.tag = indexPath.row;
    [cell.editBtn addTarget:self action:@selector(editFertilizer:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.buyBtn.tag = indexPath.row;
    [cell.buyBtn addTarget:self action:@selector(buyFertilizer:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    
    
    return cell;
    
    
}

-(void) editFertilizer:(UIButton*)button{
    
    fertilizerWasAddedToLand=NO;
    
    int row = button.tag; //you know that which row button is tapped
    NSLog( [NSString stringWithFormat:@"%d", row ]);
    
    FertilizerDataItem * fertilizer = [[FertilizerDataItem alloc]init];
    
    fertilizer = [MatchResultFertilizers objectAtIndex:row];
    
    
    
    
  
    
    fertilizerWasAddedToLand =  [manager addFertilizer:fertilizer._id toLand:self.landItem._id withFertAmount:fertilierAmount andWaterAmount:waterAmount andSumResult:fertilizer.amount];
    if (fertilizerWasAddedToLand)
    {
        
        UIAlertView *  customAlertView = [[UIAlertView alloc] initWithTitle:@"הוספת דשן"
                                                                    message:@"דשן הוסף בהצלחה לחלקה" delegate:self
                                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [customAlertView show];
        
    }
    else
        [TinyHelpers showAlert:@"הוספת דשן" withMessage:@"לא ניתן לשמור את המידע"];
    
}







- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    [self _doGoBack];
}



-(void)buyFertilizer:(UIButton*)button{
    
    fertilizerWasAddedToLand=NO;
    
    int row = button.tag; //you know that which row button is tapped
    NSLog( [NSString stringWithFormat:@"%d", row ]);
    
    selectedFertilizer = [[FertilizerDataItem alloc]init];
    
    selectedFertilizer = [MatchResultFertilizers objectAtIndex:row];
    
    
    
    
    
    // open order view and enter details to inner text view:
    
    self.orderViewTitle.text = [NSString stringWithFormat:@"%@ %@",@"דשן",selectedFertilizer.name];
    
    if (selectedFertilizer.description!=nil)
    {
        
        self.mailBodyText.text = [NSString stringWithFormat:@"%@\n\n%@ %@ %@",selectedFertilizer.description,@"יש לדשן",selectedFertilizer.amount,@"ליטרים ליום מסוג הדשן."];
        
    }
    else
    {
        self.mailBodyText.text = [NSString stringWithFormat:@"%@\n\n%@ %@ %@",@"אין תיאור לדשן הנבחר.",@"יש לדשן",selectedFertilizer.amount,@"ליטרים ליום מסוג הדשן."];
    }
    
    
    
    self.OrderFertilizerView.hidden = NO;
    
    
    /*
     UIAlertView *  customAlertView = [[UIAlertView alloc] initWithTitle:@"הוספת דשן"
     message:@"דשן הוסף בהצלחה לחלקה" delegate:self
     cancelButtonTitle:@"OK" otherButtonTitles:nil];
     [customAlertView show];
     */
    
    
    
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    // user taps expanded row
    if (selectedIndex == indexPath.row)
    {
        selectedIndex = -1;
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        return;
    }
    
    //user taps different row
    
    if (selectedIndex!=-1)
    {
        NSIndexPath * prevPath = [NSIndexPath indexPathForRow:selectedIndex inSection:0];
        selectedIndex = indexPath.row;
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:prevPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    
    //user taps new row with non expnaded
    
    selectedIndex = indexPath.row;
    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
}





- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}



-(void)viewWillDisappear:(BOOL)animated
{
    [super clearBarButtons];
}

/*
 -(void)_doGoBack
 {
 NSLog(@"back");
 [super clearBarButtons];
 [self.navigationController popViewControllerAnimated:YES];
 
 }
 
 */


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pickFertilizerBtn:(id)sender {
    //  cropOrSpecieOrStructure = @"structure";
    fertilizerOrFertilizerAmountOrFertilizerWater = @"Fertilizer";
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:nil
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    [actionSheet setTitle:@"סוג הדישון"];
    
    
    CGRect pickerFrame = CGRectMake(0, 40, 0, 0);
    
    
    
    
    
    pickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
    pickerView.showsSelectionIndicator = YES;
    pickerView.dataSource = self;
    pickerView.delegate = self;
    
    [actionSheet addSubview:pickerView];
    
    
    [pickerView reloadAllComponents] ;
    //    [pickerView selectRow:[[searchMenu objectAtIndex:selectedTableRow] selectedPickerRow ]  inComponent:0 animated:YES] ;
    
    
    
    
    
    
    UISegmentedControl *closeButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"סגור"]];
    closeButton.momentary = YES;
    closeButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
    closeButton.segmentedControlStyle = UISegmentedControlStyleBar;
    closeButton.tintColor = [UIColor blackColor];
    [closeButton addTarget:self action:@selector(dismissActionSheet:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:closeButton];
    
    
    
    UISegmentedControl *nextButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"שמור"]];
    nextButton.momentary = YES;
    nextButton.frame = CGRectMake(6, 7.0f, 50.0f, 30.0f);
    nextButton.segmentedControlStyle = UISegmentedControlStyleBar;
    nextButton.tintColor = [UIColor blackColor];
    [nextButton addTarget:self action:@selector(dismissActionSheetAndSetData:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:nextButton];
    
    [actionSheet showInView:[[UIApplication sharedApplication] keyWindow]];
    [actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
    
    
}

- (IBAction)pickFertilizerAmountBtn:(id)sender {
    //  cropOrSpecieOrStructure = @"structure";
    fertilizerOrFertilizerAmountOrFertilizerWater = @"Amount";
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:nil
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    [actionSheet setTitle:@"כמות הדשן"];
    
    
    CGRect pickerFrame = CGRectMake(0, 40, 0, 0);
    
    
    
    
    
    pickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
    pickerView.showsSelectionIndicator = YES;
    pickerView.dataSource = self;
    pickerView.delegate = self;
    
    [actionSheet addSubview:pickerView];
    
    
    [pickerView reloadAllComponents] ;
    //    [pickerView selectRow:[[searchMenu objectAtIndex:selectedTableRow] selectedPickerRow ]  inComponent:0 animated:YES] ;
    
    
    
    
    
    
    UISegmentedControl *closeButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"סגור"]];
    closeButton.momentary = YES;
    closeButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
    closeButton.segmentedControlStyle = UISegmentedControlStyleBar;
    closeButton.tintColor = [UIColor blackColor];
    [closeButton addTarget:self action:@selector(dismissActionSheet:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:closeButton];
    
    
    
    UISegmentedControl *nextButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"שמור"]];
    nextButton.momentary = YES;
    nextButton.frame = CGRectMake(6, 7.0f, 50.0f, 30.0f);
    nextButton.segmentedControlStyle = UISegmentedControlStyleBar;
    nextButton.tintColor = [UIColor blackColor];
    [nextButton addTarget:self action:@selector(dismissActionSheetAndSetData:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:nextButton];
    
    [actionSheet showInView:[[UIApplication sharedApplication] keyWindow]];
    [actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
    
    
}

- (IBAction)pickWaterAmountBtn:(id)sender {
    //  cropOrSpecieOrStructure = @"structure";
    fertilizerOrFertilizerAmountOrFertilizerWater = @"Water";
    
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:nil
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    [actionSheet setTitle:@"כמות מים במיכל"];
    
    
    CGRect pickerFrame = CGRectMake(0, 40, 0, 0);
    
    
    
    
    
    pickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
    pickerView.showsSelectionIndicator = YES;
    pickerView.dataSource = self;
    pickerView.delegate = self;
    
    [actionSheet addSubview:pickerView];
    
    
    [pickerView reloadAllComponents] ;
    //    [pickerView selectRow:[[searchMenu objectAtIndex:selectedTableRow] selectedPickerRow ]  inComponent:0 animated:YES] ;
    
    
    
    
    
    
    UISegmentedControl *closeButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"סגור"]];
    closeButton.momentary = YES;
    closeButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
    closeButton.segmentedControlStyle = UISegmentedControlStyleBar;
    closeButton.tintColor = [UIColor blackColor];
    [closeButton addTarget:self action:@selector(dismissActionSheet:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:closeButton];
    
    
    
    UISegmentedControl *nextButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"שמור"]];
    nextButton.momentary = YES;
    nextButton.frame = CGRectMake(6, 7.0f, 50.0f, 30.0f);
    nextButton.segmentedControlStyle = UISegmentedControlStyleBar;
    nextButton.tintColor = [UIColor blackColor];
    [nextButton addTarget:self action:@selector(dismissActionSheetAndSetData:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:nextButton];
    
    [actionSheet showInView:[[UIApplication sharedApplication] keyWindow]];
    [actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
    
    
}

- (IBAction) dismissActionSheetAndSetData :(id)sender
{
    
    /*
     
     
     
     NSString * fertType;
     NSString * fertilierAmount;
     NSString * waterAmount;
     
     @property (strong, nonatomic) IBOutlet UIButton *fertilizerLabel;
     @property (strong, nonatomic) IBOutlet UIButton *fertAmountLabel;
     @property (strong, nonatomic) IBOutlet UIButton *waterAmountLabel;
     
     */
    
    UIActionSheet *actionSheet =  (UIActionSheet *)[(UIView *)sender superview];
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    
    
    NSInteger row;
    
    row = [pickerView selectedRowInComponent:0];
    
    
    if ([fertilizerOrFertilizerAmountOrFertilizerWater isEqualToString:@"Fertilizer"])
    {
        
        
        //sets first picker
        
        if ([sortedFertilizerTypesArray count]>0)
        {
        
        fertType =  ((FertilizerTypeDataItem*)[sortedFertilizerTypesArray objectAtIndex:row]).name;
        [self.fertilizerLabel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.fertilizerLabel setTitle:fertType forState:UIControlStateNormal];
        
        
        
        
        
        // sets second and third pickers
        
        if ([((FertilizerTypeDataItem*)[sortedFertilizerTypesArray objectAtIndex:row]).type isEqualToString:@"נוזלי"])
        {
            
            fertilierAmount = nil;
            waterAmount = nil;
            
            [self.fertAmountLabel setEnabled:NO];
            [self.waterAmountLabel setEnabled:NO];
            
            
            [self.fertAmountLabel setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            [self.fertAmountLabel setTitle: @"בחר כמות" forState:UIControlStateNormal];
            [self.waterAmountLabel setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            [self.waterAmountLabel setTitle:@"בחר כמות" forState:UIControlStateNormal];
            
            
            
        }
        else
        {
            
            if (fertilierAmount ==nil && waterAmount ==nil)
                
            {
                fertilierAmount = [fertilizerAmountArray objectAtIndex:2];
                [self.fertAmountLabel setTitle: fertilierAmount forState:UIControlStateNormal];
                waterAmount = [fertilizerWaterArray objectAtIndex:14];
                [self.waterAmountLabel setTitle:waterAmount forState:UIControlStateNormal];
            }
            else if (fertilierAmount==nil)
            {
                fertilierAmount = [fertilizerAmountArray objectAtIndex:2];
                [self.fertAmountLabel setTitle: fertilierAmount forState:UIControlStateNormal];
            }
            
            else if (waterAmount==nil)
            {
                waterAmount = [fertilizerWaterArray objectAtIndex:14];
                [self.waterAmountLabel setTitle:waterAmount forState:UIControlStateNormal];
                
            }
            
            [self.fertAmountLabel setEnabled:YES];
            [self.waterAmountLabel setEnabled:YES];
            [self.fertAmountLabel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [self.waterAmountLabel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
        }
        
        
        // load table view with data (with the following details: specie, week, fertType and structure)
        
        MatchResultFertilizers = [FertilizerDataManager getMatchResultBySpecieID:self.landItem.specieItem._id onWeek:[TinyHelpers calculateDateDifferenceInWeeks:self.landItem.assigned_date] atStructure:self.landItem.structure MatchToFertilizerType:fertType];
        
        if (![MatchResultFertilizers count])
        {
            [TinyHelpers showAlert:@"תוצאות דישון" withMessage:@"לא נמצאו דשנים מתאימים לנתוני החיפוש"];
            self.tableView.hidden = YES;
        }
        else
        {
            self.tableView.hidden = NO;
            
        }
        
        //  [self.tableView reloadData];
        
        }
        
        
    }
    else if ([fertilizerOrFertilizerAmountOrFertilizerWater isEqualToString:@"Amount"])
    {
        
        fertilierAmount =  [fertilizerAmountArray objectAtIndex:row];
        [self.fertAmountLabel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.fertAmountLabel setTitle:fertilierAmount forState:UIControlStateNormal];
        
        
    }
    else
    {
        waterAmount =  [fertilizerWaterArray objectAtIndex:row];
        [self.waterAmountLabel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.waterAmountLabel setTitle:waterAmount forState:UIControlStateNormal];
    }
    
    
    
    
    [self reloadTableView];
    
    
    
}


- (IBAction) dismissActionSheet :(id)sender
{
    
    UIActionSheet *actionSheet =  (UIActionSheet *)[(UIView *)sender superview];
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    
}




- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1 ;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    
    if ([fertilizerOrFertilizerAmountOrFertilizerWater isEqualToString:@"Fertilizer"])
    {
        return [sortedFertilizerTypesArray count];
    }
    else if ([fertilizerOrFertilizerAmountOrFertilizerWater isEqualToString:@"Amount"])
    {
        return [fertilizerAmountArray count];
    }
    else
        return [fertilizerWaterArray count];
    
    
    //  return [sortedFertilizerTypesArray count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    
    if ([fertilizerOrFertilizerAmountOrFertilizerWater isEqualToString:@"Fertilizer"])
    {
        
        return ((FertilizerTypeDataItem *)[sortedFertilizerTypesArray objectAtIndex:row]).name;
    }
    else if ([fertilizerOrFertilizerAmountOrFertilizerWater isEqualToString:@"Amount"])
    {
        return [fertilizerAmountArray objectAtIndex:row];
    }
    else
        return [fertilizerWaterArray objectAtIndex:row];
    
    
}



- (IBAction)doSaveHistoryBtn:(id)sender {
}

-(void)reloadTableView{
    
    [self.tableView reloadData];
    
    
}
- (IBAction)closeOrderView:(id)sender {
    
    self.OrderFertilizerView.hidden = YES;
    
}

- (IBAction)MailOrderToAdmin:(id)sender {
    
    
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [HUD setLabelText:NSLocalizedString(@"...שולח הזמנה", @"...שולח הזמנה")];
    [self.view addSubview:HUD];
    
    
    [HUD showWhileExecuting:@selector(sendReportMail) onTarget:self withObject:nil animated:YES] ;
    
    
}
-(void)sendReportMail
{
    
    
    
    UserDataItem * user = [[UserDataItem alloc]init];
    user = [UserDataManager getCurrentUser];
    Email * email = [[Email alloc]init];
    email.emailTo = testEmailAddress;
    
    //  NSString * fertDetails=@"";
    //   fertDetails = [NSString stringWithFormat:@"%@,\n%@\t%@\t%@\t%@ ליטרים להזמנה",fertDetails,selectedFertilizer.name,selectedFertilizer.company,selectedFertilizer.created,selectedFertilizer.amount];
    email.message = [NSString stringWithFormat:@"מעוניין להזמין דשן %@.\n%@",selectedFertilizer.name, self.mailBodyText.text];
    email.userDetails = [NSString stringWithFormat:@"%@\t%@\t%@",user.fullName,user.email,user.phone];
    email.farmDetails = [NSString stringWithFormat:@"%@-%@\t%@\t%@",self.farmItem.name,self.farmItem.autoNumber,self.farmItem.address,self.farmItem.phone];
    email.landDetails = [NSString stringWithFormat:@"חלקת %@ - %@\t%@",self.landItem.specieItem.name,self.landItem.autoNumber,self.landItem.structure];
    email.fertilizersDetails =@"";
    
    
    
    
    
    
    // fertDetails = [fertDetails stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""];
    
    //  email.fertilizersDetails = fertDetails;
    
    BOOL emailSent = [GodzillaDataManager sendMail:email];
    if (emailSent)
        
    {
        
        fertilizerWasAddedToLand =  [manager addFertilizer:selectedFertilizer._id toLand:self.landItem._id withFertAmount:fertilierAmount andWaterAmount:waterAmount andSumResult:selectedFertilizer.amount];
        if (fertilizerWasAddedToLand)
        {
            self.OrderFertilizerView.hidden = YES;
            [TinyHelpers showAlert:@"צור קשר" withMessage:@"בקשתך נשלחה בהצלחה. ניצור איתך קשר בהקדם"];
            
            
        }
    }
    else
    {
        [TinyHelpers showAlert:@"הוספת דשן לחלקה" withMessage:@"לא ניתן להוסיף את הדשן לחלקה"];
        
        
    }
    
}

@end
