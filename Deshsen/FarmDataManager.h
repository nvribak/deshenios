//
//  FarmDataManager.h
//  Deshsen
//
//  Created by Avi Osipov on /6/414.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GodzillaDataManager.h"
#import "FarmDataItem.h"

@interface FarmDataManager : NSObject

-(NSArray *)getFarmsByUserId: (NSString *)userId;
-(BOOL)addFarm:(FarmDataItem *)farm;
-(BOOL) updateFarm : (FarmDataItem *) farm;
- (BOOL) updateFarmPhoto : (FarmDataItem *) farm;

@end
