//
//  UserDataManager.h
//  Deshsen
//
//  Created by Avi Osipov on /16/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GodzillaDataManager.h"
#import "UserDataItem.h"
#import "TinyHelpers.h"


@interface UserDataManager : NSObject{
    
    UserDataItem *currentUser ;
}
+ (BOOL) login : (NSString*) email Password : (NSString *) pass ;
+ (UserDataItem *) getCurrentUser ;
+ (void) clearUserSession ;
+ (BOOL) updateUserProfile : (UserDataItem *) user;

+(BOOL) signupUserwithEmail: (NSString *) email andName:(NSString *)name andPhone: (NSString *)phone andPassword: (NSString *) password;

@end
