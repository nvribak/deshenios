//
//  FertilizerInfoViewController.m
//  Deshsen
//
//  Created by Avi Osipov on /20/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#define emailAddress @"omrikaye@gmail.com"
#define testEmailAddress @"ran@appli.co.il"


#import "FertilizerInfoViewController.h"


@interface FertilizerInfoViewController ()

@end

@implementation FertilizerInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    manager = [[FertilizerDataManager alloc]init];
    fertilizersArray = [[NSArray alloc]init];
    self.messageText.delegate = self;
    
    // initialize species growth stages array for tableview data
    species_growth_stages = [[NSArray alloc]init];
    
    species_growth_stages = [SpecieDataManager getSpeciesGrowthStagesTable];
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = [NSString stringWithFormat:@"חלקת %@ - %@",self.landItem.specieItem.name,self.landItem.autoNumber];
    
    [super setUpBarButton];
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [HUD setLabelText:NSLocalizedString(@"טוען נתונים...", @"טוען נתונים...")];
    [self.view addSubview:HUD];
    
    
    [HUD showWhileExecuting:@selector(_loadView ) onTarget:self withObject:nil animated:YES] ;
    
    
    
}






-(void)_loadView
{
    self.specieName.text = [NSString stringWithFormat:@"%@ %@", self.landItem.cropItem.name,self.landItem.specieItem.name];
    self.structureName.text = self.landItem.structure;
    [self.switchBtn setOn:self.landItem.status];
    self.landName.text = self.landItem.cropItem.name;
    
    self.subViewTitle.text = [NSString stringWithFormat:@"חלקת %@ - %@",self.landItem.specieItem.name,self.landItem.autoNumber];
    
    fertilizersArray = [manager getFertilizersByLandId:self.landItem._id];
    if ([fertilizersArray count]<3)
        self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, 45*[fertilizersArray count]);
    [self.tableView reloadData];
    
    
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super clearBarButtons];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 44;
}

/** table view stuff goes here ***/


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [fertilizersArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    
    FertilizerDataItem *item = [[FertilizerDataItem alloc]init] ;
    item = [fertilizersArray objectAtIndex:indexPath.row ] ;
    
    
    static NSString *simpleTableIdentifier = @"FertilizerInfoCell";
    
    FertilizerInfoCell *cell = (FertilizerInfoCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FertilizerInfoCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    if (indexPath.row %2 ==0)
    {
        UIColor *myColor = [UIColor colorWithRed:(221.0 / 255.0) green:(234.0 / 255.0) blue:(204.0 / 255.0) alpha: 1];
        [cell setBackgroundColor:myColor];// = mycolor;
    }
    
    
    cell.fertilizerName.text = item.name;
    cell.fertilizerOrderDate.text = item.created;
    
    /// calc growth_stage:
    

    
    NSString * days = [TinyHelpers calculateDateDifferenceInWeeksFromDate:self.landItem.assigned_date toDate:item.created];
    
    
    
    // calculate difference bewteen the calculated date and the date in species_growth_stages array:
    
    for (specieGrowthStagesDataItem * subItem  in species_growth_stages) {
        if ([subItem.specieId isEqualToString:self.landItem.specieItem._id] && [subItem.fromDay intValue] <= [days intValue] && [subItem.toDay intValue] >=[days intValue] )
        {
            cell.timeStampLabel.text = subItem.stageName;
            item.growthStage = subItem.stageName;
            break;
        }
        
        
        else
        {
            cell.timeStampLabel.text = @"מידע לא זמין";
            item.growthStage = @"מידע לא זמין";

            
        }
    }
    
    
    
    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    FertilizerDataItem *item = [[FertilizerDataItem alloc]init] ;
    item = [fertilizersArray objectAtIndex:indexPath.row ] ;
    
    if (item.description==nil)
        item.description = @"מידע לא זמין";
    
    ///set text view data:
    
    // if its a solid fertilizer:
    
    if ([item.categoryType isEqualToString:@"מוצק"]) {
        self.moreInfoTextView.text = [NSString stringWithFormat:@"שם דשן: %@\n\nתיאור: %@\n\nכמות מים במיכל: %@\n\nכמות דשן בשקים לפי 25 ק״ג: %@\n\nשלב גידול: %@\n\nכמון דישון מומלצת: %@",item.name,item.description,item.waterAmount,item.fertAmount,item.growthStage,item.amount];
        

    }
    
  
    // if its a liquid fertilizer:
    else
    
    {
        self.moreInfoTextView.text = [NSString stringWithFormat:@"שם דשן: %@\n\nתיאור: %@\n\nשלב גידול: %@\n\nכמות דישון מומלצת: %@",item.name,item.description,item.growthStage,item.amount];
     
    
 
    }
    
   
    
    // set sub view visibile:
    self.moreInfoSubView.hidden = NO;
    
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.defaultLabel.hidden = YES;
}

- (IBAction)consultBtn:(id)sender {
    
    self.modalView.hidden  = NO;
}
- (IBAction)closeModalView:(id)sender {
    self.modalView.hidden = YES;
}

- (IBAction)editLandBtn:(id)sender {
    
    AddLandViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"NewLand"];
    
    [controller setLand:self.landItem];
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)openFertilizer:(id)sender {
    
    FertilizerViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"Fertilizer"];
    [controller setFarmItem:self.farmItem];
    [controller setLandItem:self.landItem];
    [self.navigationController pushViewController:controller animated:YES];

    
}

- (IBAction)sendMail:(id)sender {
   
    
    if (![self.messageText.text isEqualToString:@"" ])
    {
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [HUD setLabelText:NSLocalizedString(@"...שולח מייל", @"...שולח מייל")];
    [self.view addSubview:HUD];
    
    
    [HUD showWhileExecuting:@selector(sendReportMail) onTarget:self withObject:nil animated:YES] ;
    }
    else
        [TinyHelpers showAlert:@"צור קשר" withMessage:@"עליך להזין את פרטי הבקשה"];
    
}
    -(void)sendReportMail
    {
        
        UserDataItem * user = [[UserDataItem alloc]init];
        user = [UserDataManager getCurrentUser];
        Email * email = [[Email alloc]init];
        email.emailTo = testEmailAddress;
        email.message = self.messageText.text;
        email.userDetails = [NSString stringWithFormat:@"%@\t%@\t%@",user.fullName,user.email,user.phone];
        email.farmDetails = [NSString stringWithFormat:@"%@-%@\t%@\t%@",self.farmItem.name,self.farmItem.autoNumber,self.farmItem.address,self.farmItem.phone];
        email.landDetails = [NSString stringWithFormat:@"חלקת %@ - %@\t%@",self.landItem.specieItem.name,self.landItem.autoNumber,self.landItem.structure];
        
        NSString * fertDetails=@"";
        
        for (FertilizerDataItem * item in fertilizersArray) {
            fertDetails = [NSString stringWithFormat:@"%@,\n%@\t%@\t%@\n",fertDetails,item.name,item.company,item.created];
        }
        
        
        fertDetails = [fertDetails stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""];

        email.fertilizersDetails = fertDetails;
        
       BOOL emailSent = [GodzillaDataManager sendMail:email];
        if (emailSent)
            
        {
            [TinyHelpers showAlert:@"צור קשר" withMessage:@"בקשתך נשלחה בהצלחה. ניצור איתך קשר בהקדם"];
            
        }
        
    }


- (IBAction)sendSMS:(id)sender {
}



- (IBAction)closeMoreInfoSubView:(id)sender {
    
    self.moreInfoSubView.hidden = YES;
}
@end
