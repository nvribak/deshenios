//
//  MainViewController.m
//  Deshsen
//
//  Created by Avi Osipov on /16/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"החשבון שלי";
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    user = [[UserDataItem alloc]init];
    manager = [[FarmDataManager alloc]init];
    

	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated


{
    
    
    [self setUpBarButton];
    /*
    UIButton *butt=[UIButton buttonWithType:UIButtonTypeCustom ];
    [butt setFrame:CGRectMake(-10, 0, 30, 30)];
    [butt setImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    
    [butt addTarget:self action:@selector(revealMenu:) forControlEvents:UIControlEventTouchDown];
    
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:butt];

    self.navigationItem.leftBarButtonItem = leftButton;
    
    */
    
    if ([UserDataManager getCurrentUser]==nil)
    {
       // [super clearBarButtons];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LoginViewController *privacy = (LoginViewController*)[storyboard instantiateViewControllerWithIdentifier:@"Login"];
        
        // present
        [self presentViewController:privacy animated:YES completion:nil];
        
        // dismiss
        //  [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        
        
        
        MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.view];
        [HUD setLabelText:NSLocalizedString(@"טוען נתונים...", @"טוען נתונים...")];
        [self.view addSubview:HUD];
        
        
        [HUD showWhileExecuting:@selector(_loadView ) onTarget:self withObject:nil animated:YES] ;
        
       
        
        
        
    }
    
    

}


-(void)setUpBarButton
{
    self.navigationItem.hidesBackButton = YES;

    UIButton *butt2=[UIButton buttonWithType:UIButtonTypeCustom ];
    [butt2 setFrame:CGRectMake(278, 2, 40, 40)];
    [butt2 setImage:[UIImage imageNamed:@"settings_button.png"] forState:UIControlStateNormal];
    
    [butt2 addTarget:self action:@selector(_doOpenSettings) forControlEvents:UIControlEventTouchDown];
    [butt2 setTag:200];
    
    
    [self.navigationController.navigationBar addSubview:butt2];

}

-(void)_doOpenSettings
{
    [self clearBarButtons];
    EditAccountViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"EditAccount"];
    [controller setUser:[UserDataManager getCurrentUser]];
    [self.navigationController pushViewController:controller animated:YES];
    
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super clearBarButtons];

}
-(void)pp
{
    
}
-(void)_loadView
{
    NSLog(@"Logedd In");
    user = [UserDataManager getCurrentUser];
    self.usernameField.text = [UserDataManager getCurrentUser].fullName;
    farmsArray = [[NSArray alloc]init];
    farmsArray = [manager getFarmsByUserId:user._id];
    if ([farmsArray count]<3)
        self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, 41*[farmsArray count]);
    [self.tableView reloadData];
    
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 41;
}

/** table view stuff goes here ***/


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [farmsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   


    FarmDataItem *item = [[FarmDataItem alloc]init] ;
    item = [farmsArray objectAtIndex:indexPath.row ] ;
    
    
    static NSString *simpleTableIdentifier = @"FarmCell";
    
    FarmCell *cell = (FarmCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FarmCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    if (indexPath.row %2 ==0)
    {
        UIColor *myColor = [UIColor colorWithRed:(221.0 / 255.0) green:(234.0 / 255.0) blue:(204.0 / 255.0) alpha: 1];
        [cell setBackgroundColor:myColor];// = mycolor;
    }
    
    
    cell.farmNumber.text = item.autoNumber;
    cell.farmName.text = item.name;
    cell.editFarm.tag = indexPath.row;
    [cell.editFarm addTarget:self action:@selector(editFarm:) forControlEvents:UIControlEventTouchUpInside];

    
    return cell;
    
}

-(void) editFarm:(UIButton*)button{
    
    int row = button.tag; //you know that which row button is tapped
    
    FarmDataItem *item = [[FarmDataItem alloc]init] ;
    item = [farmsArray objectAtIndex:row ] ;
    
    EditFarmViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"EditFarm"];
    [controller setFarmItem:item];
    [controller setUpdateFarm:YES];
    [self.navigationController pushViewController:controller animated:YES];
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FarmDataItem *item = [[FarmDataItem alloc]init] ;
    item = [farmsArray objectAtIndex:indexPath.row ] ;
    FarmViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"Farm"];
    [controller setFarmItem:item];
    [self.navigationController pushViewController:controller animated:YES];
}


- (IBAction)addFarmBtn:(id)sender {
    
   // [super clearBarButtons];
    EditFarmViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"EditFarm"];
    
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)_doGoBack
{
    NSLog(@"back");
}
/*
-(void)_doOpenSettings
{
    [super clearBarButtons];
    EditAccountViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"EditAccount"];
    [self.navigationController pushViewController:controller animated:YES];

}
 */
-(void)_doOpenSearch
{
    NSLog(@"search");

}



@end
