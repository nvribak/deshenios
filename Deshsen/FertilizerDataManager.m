//
//  FertilizerDataManager.m
//  Deshsen
//
//  Created by Avi Osipov on /7/414.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "FertilizerDataManager.h"

@implementation FertilizerDataManager
-(NSArray *)getFertilizersByLandId: (NSString *)landId
{
    GodzillaDataManager * manager = [[GodzillaDataManager alloc]init];
    
    NSString *request = [NSString stringWithFormat:@"land_id=%@"  , landId] ;
    NSString *response = [manager sendServerRequest:@"map/fertilizer_in_land" withGetQuery:request withPostQuery:@""] ;
    
    
    
    
    /// parse json response
    
    id jsonObj = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    
    
    if ([jsonObj isKindOfClass:[NSDictionary class]]) {
        
        
        
        NSArray *resultArray = [jsonObj objectForKey:@"data"];
        NSMutableArray *responseArray = [[NSMutableArray alloc] init ] ;
        
        for (NSDictionary *item in resultArray) {
            
            FertilizerDataItem *fertItem = [[FertilizerDataItem alloc] init] ;
            
            
            
            NSMutableArray *responseArray2 = [[NSMutableArray alloc] init ] ;
            responseArray2 = [item objectForKey:@"fertilizer_id"];
            for (NSDictionary *subItem in responseArray2) {
                
                fertItem = [[FertilizerDataItem alloc] init] ;
                fertItem._id = [subItem objectForKey:@"_id"] ;
                fertItem.name = [subItem objectForKey:@"name"];
                fertItem.nField = [subItem objectForKey:@"n_field"] ;
                fertItem.pField = [subItem objectForKey:@"p_field"];
                fertItem.kField = [subItem objectForKey:@"k_field"] ;
                fertItem.micro = [subItem objectForKey:@"micro"];
                fertItem.density = [subItem objectForKey:@"density"];
                fertItem.mg = [subItem objectForKey:@"mg"] ;
                fertItem.company = [subItem objectForKey:@"company"];
                fertItem.categoryName = [subItem objectForKey:@"category_name"] ;
                fertItem.categoryType = [subItem objectForKey:@"category_type"];
                fertItem.description = [subItem objectForKey:@"description"];
                
                
            }
            
            
            NSString *date_created = [[item objectForKey:@"created"] objectForKey:@"sec"] ;
            NSString *date = [TinyHelpers convertMongoDate:date_created] ;
            fertItem.created = date;
            fertItem.fertAmount = [item objectForKey:@"fert_amount"];
            fertItem.waterAmount = [item objectForKey:@"water_amount"];
            fertItem.amount = [item objectForKey:@"sum_result"];
            
            
            [responseArray addObject:fertItem ] ;
            
            
            
        }
        
        
        
        
        return responseArray ;
        
        
    }
    
    
    return nil;
}

-(NSArray *)getFertilizers
{
    GodzillaDataManager * manager = [[GodzillaDataManager alloc]init];
    
    // NSString *request = [NSString stringWithFormat:@"land_id=%@"  , landId] ;
    NSString *response = [manager sendServerRequest:@"get/fertilizers" withGetQuery:@"" withPostQuery:@""] ;
    
    
    
    
    /// parse json response
    
    id jsonObj = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    
    
    if ([jsonObj isKindOfClass:[NSDictionary class]]) {
        
        
        
        NSArray *resultArray = [jsonObj objectForKey:@"data"];
        NSMutableArray *responseArray = [[NSMutableArray alloc] init ] ;
        
        for (NSDictionary *subItem in resultArray) {
            
            FertilizerDataItem *fertItem = [[FertilizerDataItem alloc] init] ;
            
            
            
            fertItem = [[FertilizerDataItem alloc] init] ;
            fertItem._id = [subItem objectForKey:@"_id"] ;
            fertItem.name = [subItem objectForKey:@"name"];
            fertItem.nField = [subItem objectForKey:@"n_field"] ;
            fertItem.pField = [subItem objectForKey:@"p_field"];
            fertItem.kField = [subItem objectForKey:@"k_field"] ;
            fertItem.micro = [subItem objectForKey:@"micro"];
            fertItem.density = [subItem objectForKey:@"density"];
            fertItem.mg = [subItem objectForKey:@"mg"] ;
            fertItem.company = [subItem objectForKey:@"company"];
            fertItem.categoryName = [subItem objectForKey:@"category_name"] ;
            fertItem.categoryType = [subItem objectForKey:@"category_type"];
            fertItem.description = [subItem objectForKey:@"description"];
            
            [responseArray addObject:fertItem ] ;
            
            
        }
        
        
        return responseArray ;
        
        
    }
    
    return nil;
}



-(NSArray *)getFertilizersByGrowthStageID: (NSString *)growthStageID
{
    GodzillaDataManager * manager = [[GodzillaDataManager alloc]init];
    
     NSString *request = [NSString stringWithFormat:@"growth_stage_id=%@"  , growthStageID] ;
    NSString *response = [manager sendServerRequest:@"map/match_results" withGetQuery:request withPostQuery:@""] ;
    
    
    
    
    /// parse json response
    
    id jsonObj = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    
    
    if ([jsonObj isKindOfClass:[NSDictionary class]]) {
        
        
        
        NSArray *resultArray = [jsonObj objectForKey:@"data"];
        NSMutableArray *responseArray = [[NSMutableArray alloc] init ] ;
        
        for (NSDictionary *item in resultArray) {
            
            FertilizerDataItem *fertItem = [[FertilizerDataItem alloc] init] ;
            
            
            
            NSMutableArray *responseArray2 = [[NSMutableArray alloc] init ] ;
            responseArray2 = [item objectForKey:@"fertilizer_id"];
            for (NSDictionary *subItem in responseArray2) {
                
                fertItem = [[FertilizerDataItem alloc] init] ;
                fertItem._id = [subItem objectForKey:@"_id"] ;
                fertItem.name = [subItem objectForKey:@"name"];
                fertItem.nField = [subItem objectForKey:@"n_field"] ;
                fertItem.pField = [subItem objectForKey:@"p_field"];
                fertItem.kField = [subItem objectForKey:@"k_field"] ;
                fertItem.micro = [subItem objectForKey:@"micro"];
                fertItem.density = [subItem objectForKey:@"density"];
                fertItem.mg = [subItem objectForKey:@"mg"] ;
                fertItem.company = [subItem objectForKey:@"company"];
                fertItem.categoryName = [subItem objectForKey:@"category_name"] ;
                fertItem.categoryType = [subItem objectForKey:@"category_type"];
                fertItem.description = [subItem objectForKey:@"description"];
                
                
            }
            
            
            NSString *date_created = [[item objectForKey:@"created"] objectForKey:@"sec"] ;
            NSString *date = [TinyHelpers convertMongoDate:date_created] ;
            fertItem.created = date;
            
            
            [responseArray addObject:fertItem ] ;
            
            
            
        }
        
        
        
        
        return responseArray ;
        
        
    }
    
    
    return nil;
}



-(NSString *)getGrowthStageIdBySpecieId: (NSString *)specieID andWeek : (NSString *)week andStructure: (NSString *)structure
{
    GodzillaDataManager * manager = [[GodzillaDataManager alloc]init];
    
    // NSString *request = [NSString stringWithFormat:@"land_id=%@"  , landId] ;
    NSString *request = [NSString stringWithFormat:@"specie_id=%@&week=%@&structure=%@"
                         ,specieID,week,structure] ;

    NSString *response = [manager sendServerRequest:@"get/growth_stages" withGetQuery:request withPostQuery:@""] ;
    
    
    
    
    /// parse json response
    
    id jsonObj = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    
    
    if ([jsonObj isKindOfClass:[NSDictionary class]]) {
        
        
        
        NSArray *resultArray = [jsonObj objectForKey:@"data"];
     //   NSMutableArray *responseArray = [[NSMutableArray alloc] init ] ;
        
        for (NSDictionary *subItem in resultArray) {
            
            FertilizerDataItem *fertItem = [[FertilizerDataItem alloc] init] ;
            
            
            
            fertItem = [[FertilizerDataItem alloc] init] ;
            fertItem._id = [subItem objectForKey:@"_id"] ;
            fertItem.name = [subItem objectForKey:@"name"];
            fertItem.nField = [subItem objectForKey:@"n_field"] ;
            fertItem.pField = [subItem objectForKey:@"p_field"];
            fertItem.kField = [subItem objectForKey:@"k_field"] ;
            fertItem.micro = [subItem objectForKey:@"micro"];
            fertItem.density = [subItem objectForKey:@"density"];
            fertItem.mg = [subItem objectForKey:@"mg"] ;
            fertItem.company = [subItem objectForKey:@"company"];
            fertItem.categoryName = [subItem objectForKey:@"category_name"] ;
            fertItem.categoryType = [subItem objectForKey:@"category_type"];
            fertItem.description = [subItem objectForKey:@"description"];
            
          //  [responseArray addObject:fertItem ] ;
            return  fertItem._id;
            
            
        }
        
        
       // return responseArray  ;
        
        
    }
    
    return nil;
}



+(NSArray *)getMatchResultBySpecieID: (NSString *)specieID onWeek: (NSString *)week  atStructure: (NSString * )structure MatchToFertilizerType: (NSString *) fertilizerCompanyType
{
    
    GodzillaDataManager *manager = [[GodzillaDataManager alloc] init] ;
    
    
    NSString *postQuery ;//= @"mapping=[{\"from\" : \"author_id\" , \"to\" : \"users\" }]" ;
    
    postQuery =   [ NSString stringWithFormat: @"specie_id=%@&week=%@&structure=%@&fert_type_name=%@"
                   ,specieID,week,structure,fertilizerCompanyType] ;
    
   // postQuery =   [ NSString stringWithFormat: @"specie_id=%@&week=%@&structure=%@&fert_type_name=%@"
      //             ,specieID,week,nil,nil] ;
    
    NSLog(@"mapping request: %@" , postQuery ) ;
    
    
    NSString *response = [manager sendServerRequest2:@"search_for_fertilizers" withGetQuery:@"" withPostQuery:postQuery] ;
    
    
    
    /// parse json response
    
    id jsonObj = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    
    
    /// generate result array
    
    NSArray *resultArray = [jsonObj objectForKey:@"data"];
    NSMutableArray *responseArray = [[NSMutableArray alloc] init ] ;
    
    
    for (NSDictionary *item in resultArray) {
        
        
        FertilizerDataItem *fertilizer = [[FertilizerDataItem alloc] init] ;
        
        
        fertilizer.nitrogen = [[item objectForKey:@"nitrogen"] stringValue] ;
        
        id fertilizerObj = [item objectForKey:@"fertilizer"];
        
   
            fertilizer._id = [[fertilizerObj objectForKey:@"_id"] objectForKey:@"$id"];
            fertilizer.name = [fertilizerObj objectForKey:@"name"]  ;
            fertilizer.nField = [fertilizerObj objectForKey:@"n_field"]  ;
            fertilizer.pField = [fertilizerObj objectForKey:@"p_field"]  ;
            fertilizer.kField = [fertilizerObj objectForKey:@"k_field"]  ;
            fertilizer.micro = [fertilizerObj objectForKey:@"micro"]  ;
            fertilizer.density = [fertilizerObj objectForKey:@"density"];
            fertilizer.mg = [fertilizerObj objectForKey:@"mg"] ;
            fertilizer.company = [fertilizerObj objectForKey:@"company"]  ;
            fertilizer.categoryName = [fertilizerObj objectForKey:@"category_name"]  ;
            fertilizer.categoryType = [fertilizerObj objectForKey:@"category_type"] ;
            fertilizer.description = [fertilizerObj objectForKey:@"description"] ;
        
           
            
        
        
        
            
        [responseArray addObject:fertilizer ] ;
        
        
        
    }
    
    
    
    
    
    
    
    
    
    return responseArray ;

    
}

-(BOOL)addFertilizer:(NSString *)fertilizerID toLand: (NSString *)landID withFertAmount : (NSString *)fertAmount andWaterAmount: (NSString *)waterAmount andSumResult :(NSString *) sum
{
GodzillaDataManager *manager = [[GodzillaDataManager alloc] init] ;

NSString *postQuery =@"";
postQuery =   [ NSString stringWithFormat: @"&land_id=%@&fertilizer_id=%@&fert_amount=%@&water_amount=%@&sum_result=%@",landID,fertilizerID,fertAmount,waterAmount,sum];

NSString * result =  [manager sendServerRequest:@"add/fertilizer_in_land" withGetQuery:@"" withPostQuery:postQuery] ;

/// parse json response

id jsonObj = [NSJSONSerialization JSONObjectWithData:[result dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];


NSLog(@"response : %@" , result ) ;
id dataObj = [jsonObj objectForKey:@"success"] ;

/// generate result array



BOOL value =  [[dataObj objectForKey:@"ok"] boolValue]  ;
if (value)
return YES;
return NO;

}

@end
