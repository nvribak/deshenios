//
//  FarmViewController.h
//  Deshsen
//
//  Created by Avi Osipov on /18/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "KeyboardSupportViewController.h"
#import "MBProgressHUD.h"
#import "LandCell.h"
#import "AddLandViewController.h"
#import "EditFarmViewController.h"
#import "FertilizerInfoViewController.h"
#import "FarmDataManager.h"
#import "FarmDataItem.h"
#import "CropDataItem.h"
#import "SpecieDataItem.h"
#import "LandDataManager.h"
#import "LandDataItem.h"
#import "TinyHelpers.h"
#import "CameraViewController.h"

@interface FarmViewController : KeyboardSupportViewController <UITableViewDataSource,UITableViewDelegate>{
    LandDataManager * manager;
    NSArray * landsArray;
    
}
- (IBAction)displayFarmPhoto:(id)sender;
- (IBAction)closeViewBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *subView;
@property (strong, nonatomic) IBOutlet UIImageView *largeFarmPhoto;

@property (strong, nonatomic) IBOutlet UIImageView *farmImage;

@property (strong, nonatomic) FarmDataItem * farmItem;
@property (strong, nonatomic) IBOutlet UILabel *farmNameLabel;
- (IBAction)addLandBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)takePicture:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *usernameLabel;

@end
