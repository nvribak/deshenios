//
//  AddLandViewController.m
//  Deshsen
//
//  Created by Avi Osipov on /18/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "AddLandViewController.h"

@interface AddLandViewController ()

@end

@implementation AddLandViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]]];
    self.scrollView.frame = CGRectMake(0, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height);
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, 580);
    self.scrollView.delegate = self;
    
    structuresArray = [[NSArray alloc]initWithObjects:@"בית רשת",@"חממה",@"מנהרה",@"שטח פתוח", nil];
    cropsArray = [[NSArray alloc]init];
    cropManager = [[CropDataManager alloc]init];
    specieManager = [[SpecieDataManager alloc]init];
    
    cropsArray = [cropManager getAllCrops:nil];
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [super setUpBarButton];
    
    
    self.navigationItem.title = @"חלקה חדשה";
    
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [HUD setLabelText:NSLocalizedString(@"טוען נתונים...", @"טוען נתונים...")];
    [self.view addSubview:HUD];
    
    
    [HUD showWhileExecuting:@selector(_loadView ) onTarget:self withObject:nil animated:YES] ;
    
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super clearBarButtons];
}

-(void)_loadView
{
    
    
    if (!self.cameFromFarm)
    {
        
        /*
         @interface LandDataItem : NSObject
         @property (strong,nonatomic) NSString * _id;
         @property (strong,nonatomic) NSString * autoNumber;
         @property (strong,nonatomic) NSString * details;
         @property (strong,nonatomic) NSString * structure;
         @property (nonatomic) BOOL  status;
         @property (strong,nonatomic) NSString * farmId;
         @property (strong,nonatomic) NSString * assigned_date;
         @property (strong,nonatomic) SpecieDataItem * specieItem;
         @property (strong,nonatomic) CropDataItem * cropItem;
         */
        
        
        self.navigationItem.title = [NSString stringWithFormat:@"חלקת %@ - %@",self.land.specieItem.name,self.land.autoNumber];
        
        
        for (int i = 0; i< [structuresArray count];i++) {
            if ([structuresArray[i] isEqualToString:self.land.structure])
            {
                selectedStructure =  [structuresArray objectAtIndex:i];
                [structureLabel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                [structureLabel setTitle:selectedStructure forState:UIControlStateNormal];
                break;
            }
        }
        
        for (CropDataItem * crop in cropsArray) {
            
            if ([crop.name isEqualToString:self.land.cropItem.name])
            {
                
                selectedCrop = [[CropDataItem alloc]init];
                selectedCrop =  crop;
                [cropLabel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                [cropLabel setTitle:selectedCrop.name forState:UIControlStateNormal];
                
                
                
                
                break;
                
            }
            
        }
        
        
        speciesArray = [[NSArray alloc]init];
        speciesArray = [specieManager getSpeciesByCropId:selectedCrop._id];
        
        
        for (SpecieDataItem * specie in speciesArray) {
            
            if ([specie.name isEqualToString:self.land.specieItem.name])
            {
                
                selectedSpecie = [[SpecieDataItem alloc]init];
                
                selectedSpecie =  specie;
                [specieLabel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                [specieLabel setTitle:selectedSpecie.name forState:UIControlStateNormal];
                
            }
        }
        
        
        NSArray * dateArray = [[NSArray alloc]init];
        
        dateArray =[self.land.assigned_date componentsSeparatedByString:@"/"];
        
        
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = [[NSDateComponents alloc] init];
        [components setYear:[dateArray[2] intValue] ];
        [components setMonth:[dateArray[1] intValue]];
        [components setDay:[dateArray[0] intValue]];
        NSDate *defualtDate = [calendar dateFromComponents:components];
        self.datePicker.date  = defualtDate;
        
        
        NSURL *imageURL = [NSURL URLWithString:self.land.photo];
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        UIImage *image = [UIImage imageWithData:imageData];
        [self.landPreviewImage setImage:image];
        
        
        
    }
    if ([[SingleProjectManager sharedProject] getPhotoImage]!=nil)
        [self.landPreviewImage setImage:[[SingleProjectManager sharedProject] getPhotoImage]];
    
    
}


/*
 -(void)_doGoBack
 {
 NSLog(@"back");
 [super clearBarButtons];
 [self.navigationController popViewControllerAnimated:YES];
 
 }
 */
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}








-(IBAction)pickCropButton:(id)sender
{
    
    cropOrSpecieOrStructure = @"crop";
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:nil
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    [actionSheet setTitle:@"סוג גידול"];
    
    
    CGRect pickerFrame = CGRectMake(0, 40, 0, 0);
    
    
    
    
    
    pickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
    pickerView.showsSelectionIndicator = YES;
    pickerView.dataSource = self;
    pickerView.delegate = self;
    
    [actionSheet addSubview:pickerView];
    
    
    [pickerView reloadAllComponents] ;
    //    [pickerView selectRow:[[searchMenu objectAtIndex:selectedTableRow] selectedPickerRow ]  inComponent:0 animated:YES] ;
    
    
    
    
    
    
    UISegmentedControl *closeButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"סגור"]];
    closeButton.momentary = YES;
    closeButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
    closeButton.segmentedControlStyle = UISegmentedControlStyleBar;
    closeButton.tintColor = [UIColor blackColor];
    [closeButton addTarget:self action:@selector(dismissActionSheet:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:closeButton];
    
    
    
    UISegmentedControl *nextButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"שמור"]];
    nextButton.momentary = YES;
    nextButton.frame = CGRectMake(6, 7.0f, 50.0f, 30.0f);
    nextButton.segmentedControlStyle = UISegmentedControlStyleBar;
    nextButton.tintColor = [UIColor blackColor];
    [nextButton addTarget:self action:@selector(dismissActionSheetAndSetData:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:nextButton];
    
    [actionSheet showInView:[[UIApplication sharedApplication] keyWindow]];
    [actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
}

- (IBAction)pickStructureBtn:(id)sender {
    
    cropOrSpecieOrStructure = @"structure";
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:nil
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    [actionSheet setTitle:@"סוג מבנה"];
    
    
    CGRect pickerFrame = CGRectMake(0, 40, 0, 0);
    
    
    
    
    
    pickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
    pickerView.showsSelectionIndicator = YES;
    pickerView.dataSource = self;
    pickerView.delegate = self;
    
    [actionSheet addSubview:pickerView];
    
    
    [pickerView reloadAllComponents] ;
    //    [pickerView selectRow:[[searchMenu objectAtIndex:selectedTableRow] selectedPickerRow ]  inComponent:0 animated:YES] ;
    
    
    
    
    
    
    UISegmentedControl *closeButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"סגור"]];
    closeButton.momentary = YES;
    closeButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
    closeButton.segmentedControlStyle = UISegmentedControlStyleBar;
    closeButton.tintColor = [UIColor blackColor];
    [closeButton addTarget:self action:@selector(dismissActionSheet:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:closeButton];
    
    
    
    UISegmentedControl *nextButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"שמור"]];
    nextButton.momentary = YES;
    nextButton.frame = CGRectMake(6, 7.0f, 50.0f, 30.0f);
    nextButton.segmentedControlStyle = UISegmentedControlStyleBar;
    nextButton.tintColor = [UIColor blackColor];
    [nextButton addTarget:self action:@selector(dismissActionSheetAndSetData:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:nextButton];
    
    [actionSheet showInView:[[UIApplication sharedApplication] keyWindow]];
    [actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
    
    
}


-(IBAction)pickKindBtn:(id)sender
{
    
    
    if (selectedCrop!=nil)
    {
        
        cropOrSpecieOrStructure = @"specie";
        
        speciesArray = [[NSArray alloc]init];
        speciesArray = [specieManager getSpeciesByCropId:selectedCrop._id];
        
        if ([speciesArray count]>0)
        {
            
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                     delegate:nil
                                                            cancelButtonTitle:nil
                                                       destructiveButtonTitle:nil
                                                            otherButtonTitles:nil];
            
            [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
            
            [actionSheet setTitle:@"סוג זן"];
            
            
            CGRect pickerFrame = CGRectMake(0, 40, 0, 0);
            
            
            
            
            
            pickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
            pickerView.showsSelectionIndicator = YES;
            pickerView.dataSource = self;
            pickerView.delegate = self;
            
            [actionSheet addSubview:pickerView];
            
            
            [pickerView reloadAllComponents] ;
            //    [pickerView selectRow:[[searchMenu objectAtIndex:selectedTableRow] selectedPickerRow ]  inComponent:0 animated:YES] ;
            
            
            
            
            
            UISegmentedControl *closeButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"סגור"]];
            closeButton.momentary = YES;
            closeButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
            closeButton.segmentedControlStyle = UISegmentedControlStyleBar;
            closeButton.tintColor = [UIColor blackColor];
            [closeButton addTarget:self action:@selector(dismissActionSheet:) forControlEvents:UIControlEventValueChanged];
            [actionSheet addSubview:closeButton];
            
            
            
            UISegmentedControl *nextButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"שמור"]];
            nextButton.momentary = YES;
            nextButton.frame = CGRectMake(6, 7.0f, 50.0f, 30.0f);
            nextButton.segmentedControlStyle = UISegmentedControlStyleBar;
            nextButton.tintColor = [UIColor blackColor];
            [nextButton addTarget:self action:@selector(dismissActionSheetAndSetData:) forControlEvents:UIControlEventValueChanged];
            [actionSheet addSubview:nextButton];
            
            [actionSheet showInView:[[UIApplication sharedApplication] keyWindow]];
            [actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
        }
        else
            [TinyHelpers showAlert:@"בחירת זן" withMessage:@"לא נמצאו זנים לגידול הנבחר"];
    }
    else
        [TinyHelpers showAlert:@"בחירת זן" withMessage:@"עליך לבחור גידול"];
    
    
}


- (IBAction) dismissActionSheetAndSetData :(id)sender
{
    
    UIActionSheet *actionSheet =  (UIActionSheet *)[(UIView *)sender superview];
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    
    
    NSInteger row;
    
    row = [pickerView selectedRowInComponent:0];
    
    
    if ([cropOrSpecieOrStructure isEqualToString:@"crop"])
    {
        
        
        
        selectedCrop = [[CropDataItem alloc]init];
        selectedCrop =  [cropsArray objectAtIndex:row];
        [cropLabel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [cropLabel setTitle:selectedCrop.name forState:UIControlStateNormal];
        
        selectedSpecie =  nil;
        [specieLabel setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [specieLabel setTitle:@"בחר זן" forState:UIControlStateNormal];
        
        
        
    }
    else if ([cropOrSpecieOrStructure isEqualToString:@"specie"])
    {
        
        selectedSpecie = [[SpecieDataItem alloc]init];
        
        selectedSpecie =  [speciesArray objectAtIndex:row];
        [specieLabel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [specieLabel setTitle:selectedSpecie.name forState:UIControlStateNormal];
        
        
    }
    else
    {
        selectedStructure =  [structuresArray objectAtIndex:row];
        [structureLabel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [structureLabel setTitle:selectedStructure forState:UIControlStateNormal];
    }
    
    
    
    
    
    
    
}


- (IBAction) dismissActionSheet :(id)sender
{
    
    UIActionSheet *actionSheet =  (UIActionSheet *)[(UIView *)sender superview];
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    
}




- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1 ;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    
    if ([cropOrSpecieOrStructure isEqualToString:@"crop"])
    {
        return [cropsArray count];
    }
    else if ([cropOrSpecieOrStructure isEqualToString:@"specie"])
    {
        return [speciesArray count];
    }
    else
        return [structuresArray count];
    
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    // NSString * name = [cropArray objectAtIndex:row];
    if ([cropOrSpecieOrStructure isEqualToString:@"crop"])
    {
        
        return ((CropDataItem *)[cropsArray objectAtIndex:row]).name;
    }
    else if ([cropOrSpecieOrStructure isEqualToString:@"specie"])
    {
        return ((SpecieDataItem *)[speciesArray objectAtIndex:row]).name;
    }
    else
        return [structuresArray objectAtIndex:row];
    
}

/*
 
 - (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
 
 
 
 
 ProjectDataItem *item = [projectsArray objectAtIndex:row];
 
 
 
 
 UIView *v=[[UIView alloc]
 initWithFrame:CGRectMake(0,0,289,25)];
 [v setOpaque:TRUE];
 [v setBackgroundColor:[UIColor whiteColor]];
 
 
 
 
 
 
 UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(80, 0, 160, 25)];
 
 if (item.highlightItem)
 label.textColor = [UIColor greenColor];
 else
 label.textColor = [UIColor blackColor];
 
 label.backgroundColor = [UIColor whiteColor];
 label.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
 label.text = [NSString stringWithFormat:@"%@", item.title];
 label.textAlignment = UITextAlignmentRight;
 
 if (!item.unCheckItem)
 {
 UIImage *image = [UIImage imageNamed:@"v_sign_3.png"];
 
 UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
 imageView.frame = CGRectMake( 250  ,0 ,15,25) ;
 
 [v addSubview:imageView];
 }
 
 [v addSubview:label];
 return v;
 }
 
 
 */

- (IBAction)addPhotoToCrop:(id)sender {
    /*
     self.tempLand = [[LandDataItem alloc]init];
     self.tempLand.structure = selectedStructure;
     self.tempLand.cropItem = selectedCrop;
     self.tempLand.specieItem = selectedSpecie;
     self.tempLand.assigned_date = [TinyHelpers convertDateToString: self.datePicker.date ];
     */
    CameraViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"Camera"];
    [controller setCameFromNewLand:YES];
    
    [self.navigationController pushViewController:controller animated:YES];
    
    
    
}

- (IBAction)saveNewLand:(id)sender {
    
    
    
    
    
    
    BOOL canBeSaved=YES;
    
    
    if (selectedStructure ==nil)
    {
        [TinyHelpers showAlert:@"שמירת חלקה חדשה" withMessage:@"עליך לבחור סוג מבנה"];
        canBeSaved = NO;
    }
    
    if (selectedCrop==nil)
    {
        [TinyHelpers showAlert:@"שמירת חלקה חדשה" withMessage:@"עליך לבחור סוג מבנה"];
        canBeSaved = NO;
    }
    
    
    if (selectedSpecie==nil)
    {
        [TinyHelpers showAlert:@"שמירת חלקה חדשה" withMessage:@"עליך לבחור סוג מבנה"];
        canBeSaved = NO;
    }
    
    if (canBeSaved)
    {
        
        MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.view];
        [HUD setLabelText:NSLocalizedString(@"שומר חלקה...", @"שומר חלקה...")];
        [self.view addSubview:HUD];
        
        [HUD showWhileExecuting:@selector(_saveLand:) onTarget:self withObject:self.tempLand animated:YES] ;
        
        
        
        
        
    }
    
    
    
    
    
}

-(void)_saveLand: (id)sender
{
    GodzillaDataManager * manager = [[GodzillaDataManager alloc]init];
    
    LandDataManager * landManager = [[LandDataManager alloc]init];
    
    GodzillaFileItem *fileInfo = [manager uploadImage:[[SingleProjectManager sharedProject] getPhotoImage] withTitle:@"land_image"];
    
    NSLog(@"new file url : %@" , fileInfo.url ) ;
    
    LandDataItem * land = [[LandDataItem alloc]init];
    land.structure = selectedStructure;
    land.specieItem = selectedSpecie;
    land.cropItem = selectedCrop;
    land.photo = fileInfo.url;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    int year   =    [[calendar components:NSYearCalendarUnit    fromDate:[self.datePicker date]] year];
    int month  =    [[calendar components:NSMonthCalendarUnit   fromDate:[self.datePicker date]] month];
    int day    =    [[calendar components:NSDayCalendarUnit     fromDate:[self.datePicker date]] day];
    
    NSString *date = [NSString stringWithFormat:@"%d/%d/%d",day, month, year];
    
    land.assigned_date = date;
    land.status = NO;
    land.farmId = self.land.farmId;
    
    
    newLandItem = [[LandDataItem alloc]init];
    newLandItem =  [landManager addLand:land];
    
    if (newLandItem!=nil)
    {
        
        UIAlertView *  customAlertView = [[UIAlertView alloc] initWithTitle:@"הוספת חלקה"
                                                                    message:@"חלקה הוספה בהצלחה לחווה." delegate:self
                                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [customAlertView show];
        
    }
    
    //   [TinyHelpers showAlert:@"הוספת חלקה" withMessage:@"חלקה הוספה לחווה בהצלחה"];
    
    else
        [TinyHelpers showAlert:@"הוספת חלקה" withMessage:@"לא ניתן להוסיף חלקה לחווה. נסה שנית"];
    
    /*
     FertilizerViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"Fertilizer"];
     [controller setLandItem:newLandItem];
     [self.navigationController pushViewController:controller animated:YES];
     */
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (newLandItem)
    {
        
        FertilizerViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"Fertilizer"];
        [controller setLandItem:newLandItem];
        [self.navigationController pushViewController:controller animated:YES];
    }
}


/*
 -(void)_updateLand:(id)sender
 {
 
 
 
 
 GodzillaDataManager * manager = [[GodzillaDataManager alloc]init];
 
 LandDataManager * landManager = [[LandDataManager alloc]init];
 
 GodzillaFileItem *fileInfo = [manager uploadImage:[[SingleProjectManager sharedProject] getPhotoImage] withTitle:@"land_image"];
 
 NSLog(@"new file url : %@" , fileInfo.url ) ;
 
 
 
 
 
 LandDataItem * land = [[LandDataItem alloc]init];
 land.structure = selectedStructure;
 land.specieItem = selectedSpecie;
 land.cropItem = selectedCrop;
 land.photo = fileInfo.url;
 
 NSCalendar *calendar = [NSCalendar currentCalendar];
 int year   =    [[calendar components:NSYearCalendarUnit    fromDate:[self.datePicker date]] year];
 int month  =    [[calendar components:NSMonthCalendarUnit   fromDate:[self.datePicker date]] month];
 int day    =    [[calendar components:NSDayCalendarUnit     fromDate:[self.datePicker date]] day];
 
 NSString *date = [NSString stringWithFormat:@"%d/%d/%d",day, month, year];
 
 land.assigned_date = date;
 land.farmId = self.land.farmId;
 
 BOOL response =  [landManager updateLand:land];
 
 if (response)
 [TinyHelpers showAlert:@"הוספת חלקה" withMessage:@"חלקה הוספה לחווה בהצלחה"];
 
 else
 [TinyHelpers showAlert:@"הוספת חלקה" withMessage:@"לא ניתן להוסיף חלקה לחווה. נסה שנית"];
 
 
 FertilizerViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"Fertilizer"];
 [controller setLandItem:land];
 [self.navigationController pushViewController:controller animated:YES];
 
 
 }
 
 */



- (IBAction)enlargeImage:(id)sender {
    
    [self.largePhoto setImage:self.landPreviewImage.image];
    
    self.photoSubView.hidden = NO;
    
    
    self.photoSubView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        self.photoSubView.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
    }];
    
    
}
- (IBAction)closeSubView:(id)sender {
    self.photoSubView.hidden  = YES;
}
@end
