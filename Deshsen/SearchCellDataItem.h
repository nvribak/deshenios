//
//  SearchCellDataItem.h
//  ChamberApp
//
//  Created by Avi Osipov on /27/1113.
//
//

#import <Foundation/Foundation.h>

@interface SearchCellDataItem : NSObject


@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *selectedValue;
@property (nonatomic, strong) NSString *selectedValueKey;
@property (nonatomic, assign) int selectedPickerRow ;
@property (nonatomic ,assign) BOOL isEnabled ;




- (id) initWithTitle:(NSString *)title ; 



@end
