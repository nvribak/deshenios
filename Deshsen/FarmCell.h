//
//  FarmCell.h
//  Deshsen
//
//  Created by Avi Osipov on /16/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FarmCell : UITableViewCell
- (IBAction)showLinkBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *farmNumber;
@property (strong, nonatomic) IBOutlet UILabel *farmName;
@property (strong, nonatomic) IBOutlet UIButton *editFarm;

@end
