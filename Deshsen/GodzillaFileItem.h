//
//  GodzillaFileItem.h
//  Deshsen
//
//  Created by Avi Osipov on /16/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GodzillaFileItem : NSObject

@property (strong,nonatomic) NSString *_id ;
@property (strong,nonatomic) NSString *url ;
@property (strong,nonatomic) NSString *title ;
@property (strong,nonatomic) NSString *app ;
@property (strong,nonatomic) NSString *description ;


@end
