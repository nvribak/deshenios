//
//  KeyboardSupportViewController.h
//  Deshsen
//
//  Created by Avi Osipov on /16/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditAccountViewController.h"
#import <MapKit/MapKit.h>

@interface KeyboardSupportViewController : UIViewController <UITextFieldDelegate,MKMapViewDelegate,MKAnnotation>
-(void)_doGoBack;
-(void)_doOpenSettings;
-(void)_doOpenSearch;
-(void)setUpBarButton;
-(void)clearBarButtons;
- (CLLocationCoordinate2D) geoCodeUsingAddress:(NSString *)address;

@end
