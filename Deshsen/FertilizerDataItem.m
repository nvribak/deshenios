//
//  FertilizerDataItem.m
//  Deshsen
//
//  Created by Avi Osipov on /7/414.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "FertilizerDataItem.h"

@implementation FertilizerDataItem

- (id)init {
    self = [super init];
    if (self) {
        [self initHelper];
    }
    return self;
}

- (void) initHelper {
    // Custom initialization
    self._id = @"0";
    self.name = @"מידע לא זמין";

    self.nField = @"0";

    self.pField = @"0";

    self.kField = @"0";

    self.micro = @"0";

    self.density = @"0";

    self.mg = @"0";

    self.company = @"מידע לא זמין";

    self.categoryName = @"מידע לא זמין";

    self.description = @"מידע לא זמין";
    
    self.nitrogen = @"0";

    self.amount = @"0";

    self.waterAmount =@"0";

    self.fertAmount = @"0";

    self.growthStage = @"מידע לא זמין";

    self.created = @"0";

    self.updated = @"0";

    

    
    
}

@end
