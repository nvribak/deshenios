//
//  FarmViewController.m
//  Deshsen
//
//  Created by Avi Osipov on /18/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "FarmViewController.h"

@interface FarmViewController ()

@end

@implementation FarmViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    manager = [[LandDataManager alloc]init];
    landsArray = [[NSArray alloc]init];
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super setUpBarButton];
    self.navigationItem.title = [NSString stringWithFormat:@"%@ - %@",self.farmItem.name,self.farmItem.autoNumber];
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [HUD setLabelText:NSLocalizedString(@"טוען נתונים...", @"טוען נתונים...")];
    [self.view addSubview:HUD];
    
    
    [HUD showWhileExecuting:@selector(_loadView ) onTarget:self withObject:nil animated:YES] ;
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super clearBarButtons];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)_loadView
{
    self.usernameLabel.text = [UserDataManager getCurrentUser].fullName;
    self.farmNameLabel.text = self.farmItem.name;
    landsArray = [manager getLandsByFarmId:self.farmItem._id];
    if ([landsArray count]<3)
        self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, 41*[landsArray count]);
    [self.tableView reloadData];
    
    if (self.farmItem.photo!=nil)
    {
        NSURL *imageURL = [NSURL URLWithString:self.farmItem.photo];
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        UIImage *image = [UIImage imageWithData:imageData];
        
        
        
        //  self.farmImage.image = [self imageWithImage:image convertToSize:CGSizeMake(280.0, 86.0)];
        
        //  CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], CGRectMake(150, 150, 280.0 , 86.0));
        //image = [UIImage imageWithCGImage:imageRef];
        image = [self imageByCroppingImage:image toSize:CGSizeMake(280, 280)];
        self.farmImage.image = image;
    }
}

- (UIImage *)imageByCroppingImage:(UIImage *)image toSize:(CGSize)size
{
    double x = (image.size.width - size.width) / 2.0;
    double y = (image.size.height - size.height) / 2.0;
    
    CGRect cropRect = CGRectMake(x, y, size.height, size.width);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}







- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 41;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [landsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    
    LandDataItem *item = [[LandDataItem alloc]init] ;
    item = [landsArray objectAtIndex:indexPath.row ] ;
    
    
    
    static NSString *simpleTableIdentifier = @"LandCell";
    
    LandCell *cell = (LandCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"LandCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    if (indexPath.row %2 ==0)
    {
        UIColor *myColor = [UIColor colorWithRed:(221.0 / 255.0) green:(234.0 / 255.0) blue:(204.0 / 255.0) alpha: 1];
        [cell setBackgroundColor:myColor];// = mycolor;
    }
    
    
    
    cell.landNum.text = item.autoNumber;
    cell.specieName.text = item.specieItem.name;
    cell.assignedDate.text = [TinyHelpers fixDateFormat:item.assigned_date ];
    
    cell.openCam.tag = indexPath.row;
    [cell.openCam addTarget:self action:@selector(openCamView:) forControlEvents:UIControlEventTouchUpInside];
    
    
    return cell;
    
}

-(void) openCamView:(UIButton*)button{
    
    int row = button.tag; //you know that which row button is tapped
    
    LandDataItem *item = [[LandDataItem alloc]init] ;
    
    
    item = [landsArray objectAtIndex:row ] ;
    
    if (item.photo!=nil)
    {
        NSURL *imageURL = [NSURL URLWithString:item.photo];
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        UIImage *image = [UIImage imageWithData:imageData];
        [self.largeFarmPhoto setImage:image];
        
        self.subView.hidden = NO;
        
        
        self.subView.transform = CGAffineTransformMakeScale(0.01, 0.01);
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            // animate it to the identity transform (100% scale)
            self.subView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished){
            // if you want to do something once the animation finishes, put it here
        }];
    }
    else
    {
        [TinyHelpers showAlert:@"הצג תמונה" withMessage:@"אין תמונה זמינה לחלקה"];
    }
    
    
    
    
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //  [super clearBarButtons];
    
    FertilizerInfoViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"FertilizerInfo"];
    [controller setLandItem:[landsArray objectAtIndex:indexPath.row]];
    [controller setFarmItem:self.farmItem];
    [self.navigationController pushViewController:controller animated:YES];
}


/*
 -(void)_doGoBack
 {
 NSLog(@"back");
 [super clearBarButtons];
 [self.navigationController popViewControllerAnimated:YES];
 
 }
 */




- (IBAction)addLandBtn:(id)sender {
    
    //  [super clearBarButtons];
    
    AddLandViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"NewLand"];
    
    LandDataItem * land = [[LandDataItem alloc]init];
    land.farmId = self.farmItem._id;
    [controller setLand:land];
    [controller setCameFromFarm:YES];
    [self.navigationController pushViewController:controller animated:YES];
    
}
- (IBAction)takePicture:(id)sender {
    
    CameraViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"Camera"];
    [controller setFarm:self.farmItem];
    [self.navigationController pushViewController:controller animated:YES];
}
- (IBAction)displayFarmPhoto:(id)sender {
    
    NSURL *imageURL = [NSURL URLWithString:self.farmItem.photo];
    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
    UIImage *image = [UIImage imageWithData:imageData];
    [self.largeFarmPhoto setImage:image];
    
    self.subView.hidden = NO;
    
    
    self.subView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        self.subView.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
    }];
    
    
}

- (IBAction)closeViewBtn:(id)sender {
    self.subView.hidden = YES;
}




// crop preview image


- (UIImage *)squareImageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    double ratio;
    double delta;
    CGPoint offset;
    
    //make a new square size, that is the resized imaged width
    CGSize sz = CGSizeMake(newSize.width, newSize.width);
    
    //figure out if the picture is landscape or portrait, then
    //calculate scale factor and offset
    if (image.size.width > image.size.height) {
        ratio = newSize.width / image.size.width;
        delta = (ratio*image.size.width - ratio*image.size.height);
        offset = CGPointMake(delta/2, 0);
    } else {
        ratio = newSize.width / image.size.height;
        delta = (ratio*image.size.height - ratio*image.size.width);
        offset = CGPointMake(0, delta/2);
    }
    
    //make the final clipping rect based on the calculated values
    CGRect clipRect = CGRectMake(-offset.x, -offset.y,
                                 (ratio * image.size.width) + delta,
                                 (ratio * image.size.height) + delta);
    
    
    //start a new context, with scale factor 0.0 so retina displays get
    //high quality image
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(sz, YES, 0.0);
    } else {
        UIGraphicsBeginImageContext(sz);
    }
    UIRectClip(clipRect);
    [image drawInRect:clipRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}



@end
