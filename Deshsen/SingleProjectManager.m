//
//  SingleProjectManager.m
//  Deshsen
//
//  Created by Avi Osipov on /14/414.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "SingleProjectManager.h"

@implementation SingleProjectManager

static SingleProjectManager* manager;

+ (SingleProjectManager *)sharedProject {
    if (manager == nil) {
        manager = [[super allocWithZone:NULL] init];
    }
    return manager;
}

- (id)init {
    if ( (self = [super init]) ) {
        // your custom initialization
        
        
    }
    return self;
}

- (void)setPhotoImage: (UIImage *) photoImage
{
    landPhotoImage = photoImage;
}

- (UIImage *) getPhotoImage
{
    return landPhotoImage ;
}


@end
