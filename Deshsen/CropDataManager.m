//
//  CropDataManager.m
//  Deshsen
//
//  Created by Avi Osipov on /7/414.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "CropDataManager.h"

@implementation CropDataManager

-(NSArray *)getAllCrops: (NSString *)cropId
{
    GodzillaDataManager * manager = [[GodzillaDataManager alloc]init];
    NSString *request;
    if (cropId!=nil)
         request = [NSString stringWithFormat:@"%@"  , cropId] ;
    else
        request = @"";
    NSString *response = [manager sendServerRequest:@"get/crops" withGetQuery:request withPostQuery:@""] ;
    
    
    
    
    /// parse json response
    
    id jsonObj = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    
    
    if ([jsonObj isKindOfClass:[NSDictionary class]]) {
        
        
        
        NSArray *resultArray = [jsonObj objectForKey:@"data"];
        NSMutableArray *responseArray = [[NSMutableArray alloc] init ] ;
        
        for (NSDictionary *item in resultArray) {
            
            
            CropDataItem *cropItem = [[CropDataItem alloc] init] ;
       
            
            cropItem._id = [item objectForKey:@"_id"] ;
            cropItem.name = [item objectForKey:@"name"];
            
            [responseArray addObject:cropItem ] ;
            
            
            
        }
        
        
        
        
        return responseArray ;
    }
    return nil;
}

@end
