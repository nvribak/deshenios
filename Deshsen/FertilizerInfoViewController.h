//
//  FertilizerInfoViewController.h
//  Deshsen
//
//  Created by Avi Osipov on /20/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "KeyboardSupportViewController.h"
#import "FertilizerInfoCell.h"
#import "AddLandViewController.h"
#import "LandDataItem.h"
#import "FertilizerDataItem.h"
#import "FertilizerDataManager.h"
#import "MBProgressHUD.h"
#import "FarmDataItem.h"
#import "FertilizerViewController.h"
#import "Email.h"
#import "SpecieDataManager.h"

@interface FertilizerInfoViewController : KeyboardSupportViewController <UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>{
    FertilizerDataManager * manager;
    NSArray * fertilizersArray;
    NSArray * species_growth_stages;
}

@property (strong,nonatomic) FarmDataItem * farmItem;
@property (strong,nonatomic) LandDataItem * landItem;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) IBOutlet UIView *modalView;
- (IBAction)consultBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *defaultLabel;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)closeModalView:(id)sender;
- (IBAction)editLandBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *specieName;
@property (strong, nonatomic) IBOutlet UILabel *structureName;

@property (strong, nonatomic) IBOutlet UISwitch *switchBtn;
@property (strong, nonatomic) IBOutlet UILabel *landName;
- (IBAction)openFertilizer:(id)sender;
- (IBAction)sendMail:(id)sender;
- (IBAction)sendSMS:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *messageText;
@property (strong, nonatomic) IBOutlet UILabel *subViewTitle;
@property (strong, nonatomic) IBOutlet UIView *moreInfoSubView;
@property (strong, nonatomic) IBOutlet UITextView *moreInfoTextView;
- (IBAction)closeMoreInfoSubView:(id)sender;


@end
