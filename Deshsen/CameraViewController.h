//
//  CameraViewController.h
//  Deshsen
//
//  Created by Avi Osipov on /10/414.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import "KeyboardSupportViewController.h"
#import "GodzillaDataManager.h"
#import "GodzillaFileItem.h"
#import "FarmDataManager.h"
#import "FarmDataItem.h"
#import "MBProgressHUD.h"
#import "TinyHelpers.h"
#import "LandDataItem.h"
#import "AddLandViewController.h"
#import "SingleProjectManager.h"

@interface CameraViewController : KeyboardSupportViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>{
    
    GodzillaDataManager * manager;
    FarmDataManager * farmManager;
    
}

@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@property (strong, nonatomic) IBOutlet UIButton *selectPhoto;

@property (strong, nonatomic) IBOutlet UIButton *takePhoto;

@property (strong, nonatomic) FarmDataItem  *farm;

@property (strong, nonatomic) LandDataItem *land;

@property (nonatomic) BOOL cameFromNewLand;


- (IBAction)selectPhoto:(id)sender;
@end
