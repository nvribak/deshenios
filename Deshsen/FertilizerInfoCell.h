//
//  FertilizerInfoCell.h
//  Deshsen
//
//  Created by Avi Osipov on /20/314.
//  Copyright (c) 2014 appli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FertilizerInfoCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *fertilizerName;

@property (strong, nonatomic) IBOutlet UILabel *timeStampLabel;

@property (strong, nonatomic) IBOutlet UILabel *fertilizerOrderDate;

@end
